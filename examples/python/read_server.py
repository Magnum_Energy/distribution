#! /usr/bin/python3
#
#  Example code to extract data from Magnum Reader into dictionary and print
#  You may need to install requests
#   sudo pip3 install requests
#
import requests
import json
from collections import OrderedDict
import sys
url = "http://localhost:19450"
result = requests.get(url)
if result.status_code == 200:
    server_data = result.json(object_pairs_hook=OrderedDict)
    print(json.dumps(server_data, indent=4))
else:
    print(result.text)
    sys.exit(1)
