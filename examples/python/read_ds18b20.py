#
# This requires the w1thermsensor library
# sudo pip3 install w1thermsensor
# derived from https://bigl.es/ds18b20-temperature-sensor-with-python-raspberry-pi/
#
# To use with the server modify these two lines on the conf/magnum-server.conf file
# server_readers=MagnumReader,ShellReader
# shellreader_commands="/usr/bin/python3 /home/pi/magnum/python/read_ds18b20.py"
# the actual path to the file will need to be adjusted to match your installation
#
import time
import json
from collections import OrderedDict
from w1thermsensor import W1ThermSensor
sensor = W1ThermSensor()
# reduce the number of decimal points
temps = "{:2.1f}".format(sensor.get_temperature())
data = OrderedDict()
data["WTHR_temperature"] = float(temps)
print(json.dumps(data, indent=None, separators=(',', ':')))
