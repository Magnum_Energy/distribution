#!/usr/bin/env python

"""
  Copyright (c) 2018, SunSpec Alliance
  All Rights Reserved
  This program is derived from scripts/suns.py developed by SunSpec Alliance
  This program was created by Charles Godwin magnum@godwin.ca 2019/04/10
  Changes:
    Changed to generate JSON
    Adjusted comments to match functions supported
    Truncate all floating point values to 2 deciaml places 
    Convert IP addresses to display convention xxx.xxx.xxx.xxx 
     and IPv6 equivalent

  This program was developed to support the use of SunSpec devices with 
    the Magnum Energy data reader/logger available at:
    https://gitlab.com/Magnum_Energy/distribution

  Charles Godwin waves all copyrights on this program

  How to use:
  Ensure you can use the default suns.py program avalable here:
  https://github.com/sunspec/pysunspec
  Once installed test by running scripts/suns.py

  If, and only if, that program works and provides useable results, try this program

"""

import sys
import time
import re
import socket
import struct
import json
import numbers
import sunspec.core.client as client
import sunspec.core.suns as suns
import sunspec.core.util as util
from collections import OrderedDict
from optparse import OptionParser

"""
  suns options:

      -t: transport type: tcp or rtu (default: tcp)
      -a: modbus slave address (default: 1)
      -i: ip address to use for modbus tcp (default: localhost)
      -P: port number for modbus tcp (default: 502)
      -p: serial port for modbus rtu (default: /dev/ttyUSB0)
      -b: baud rate for modbus rtu (default: 9600)
      -T: timeout, in seconds (can be fractional, such as 1.5; default: 2.0)
      -r: number of retries attempted for each modbus read

  Added options:
      -f flat JSON format name/value pairs - default is a tree
         Used by the Magnum data reader
      -c compressed JSON all on one line default is pretty print
      -s Timestamp - include time stamp  - default is no
      -w Enable password display - default is ********
      -X prefiX - prefix for all fields
"""

def flatten(values, prefix):
    newvalues = OrderedDict()
    for name in values:
        value = values[name]
        if isinstance(value, dict):
            if name == '1' or name == 'models':
                newprefix = prefix
            else:
                newprefix = prefix + name + "_"
            newvalues.update(flatten(value, newprefix))
        else:
            newvalues[prefix + name] = value
    return newvalues


if __name__ == "__main__":

    usage = 'usage: %prog [options]'
    parser = OptionParser(usage=usage)
    parser.add_option('-t', metavar='TYPE', type='choice',
                      default='tcp',
                      choices=("rtu", "tcp"),
                      help='transport type: rtu, tcp [default: tcp]')
    parser.add_option('-a', metavar='number', type='int',
                      default=1,
                      help='modbus slave address [default: 1]')
    parser.add_option('-i', metavar='HOST',
                      default='localhost',
                      help='ip address to use for modbus tcp [default: localhost]')
    parser.add_option('-P', metavar='PORT', type='int',
                      default=502,
                      help='port number for modbus tcp [default: 502]')
    parser.add_option('-p', metavar=' ',
                      default='/dev/ttyUSB0',
                      help='serial port for modbus rtu [default: /dev/ttyUSB0]')
    parser.add_option('-b', metavar=' ',
                      default=9600,
                      help='baud rate for modbus rtu [default: 9600]')
    parser.add_option('-T', metavar=' ', type='float',
                      default=2.0,
                      help='timeout, in seconds (can be fractional, such as 1.5) [default: 2.0]')
# added options
    parser.add_option('-f', action="store_true", dest="flatten", default=False,
                      help='Generate flat JSON (name/value pairs) instead of a structured tree')
    parser.add_option('-c', action="store_true", dest="compress", default=False,
                      help='compress JSON to one line')
    parser.add_option('-s', action="store_true", dest="timestamp", default=False,
                      help='Add timestamp values to JSON')
    parser.add_option('-w', action="store_true", dest="showpw", default=False,
                      help='Display passwords in generated JSON')
    parser.add_option('-X', metavar='PREFIX', type='string',
                      default="",
                      help='prefiX for all fields - used only with -f flat JSON (excludes timestamp info) [default: none]')
    options, args = parser.parse_args()
    try:
        if options.t == 'tcp':
            sd = client.SunSpecClientDevice(
                client.TCP, options.a, ipaddr=options.i, ipport=options.P, timeout=options.T)
        elif options.t == 'rtu':
            sd = client.SunSpecClientDevice(
                client.RTU, options.a, name=options.p, baudrate=options.b, timeout=options.T)
        else:
            print('Unknown -t option: %s' % (options.t))
            sys.exit(1)

    except client.SunSpecClientError as e:
        print('Error: %s' % (e))
        sys.exit(1)

    if sd is None:
        sys.exit(1)
    data = OrderedDict()
    models = OrderedDict()
    data['models'] = models
    # read all models in the device
    sd.read()
    for model in sd.device.models_list:
        modeldata = OrderedDict()
        modid = str(model.id)
        tiebreaker = 1
        while modid in models:
            modid = '{0}_{1}'.format(model.id, tiebreaker)
            tiebreaker += 1
        models[modid] = modeldata
        if model.model_type.label:
            label = model.model_type.label
        else:
            label = str(model.id)
        modeldata['model'] = label
        for block in model.blocks:
            if block.index > 0:
                index = '%02d_' % (block.index)
            else:
                index = ''
            for point in block.points_list:
                if point.value is None:
                    value = None
                else:
                    if point.point_type.type == suns.SUNS_TYPE_IPADDR:
                        value = socket.inet_ntoa(struct.pack('!L', point.value))
                    elif point.point_type.type == suns.SUNS_TYPE_IPV6ADDR:
                        value = util.data_to_ipv6addr(point.value)
                    elif point.point_type.type == suns.SUNS_TYPE_EUI48:
                        value = util.data_to_eui48(point.data)
                    elif point.point_type.type == suns.SUNS_TYPE_BITFIELD16:
                        value = '0x%04x' % (point.value)
                    elif point.point_type.type == suns.SUNS_TYPE_BITFIELD32:
                        value = '0x%08x' % (point.value)
                    elif type(point.value) == float:
                        value = float("%.2f" % point.value)
                    elif type(point.value) == int:
                        value = point.value
                    else:
                        value = str(point.value).rstrip('\0')
                    if re.search('password', point.point_type.label, re.IGNORECASE) and not options.showpw:
                        value = "********"
                modeldata[index + point.point_type.id] = value
    sd.close()
    finaldata = OrderedDict()
    if options.timestamp:
        now = time.localtime()
        finaldata['Date'] = time.strftime('%Y-%m-%d %H:%M:%S', now)
        finaldata['timezone'] = time.strftime('%z', now)
    if options.flatten:
        finaldata.update(flatten(data, options.X))
    else:
        finaldata.update(data)
    if options.compress:
        print(json.dumps(finaldata, indent=None, separators=(',', ':')))
    else:
        print(json.dumps(finaldata, indent=4))
