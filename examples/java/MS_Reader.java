/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
  This file is part of ca.godwin.magnum.
  ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */ 
/* 
 * the address of the Morningstar device must befined either by changing the code in the constructor 
 * or providing a runtime parameter morningstar_address=xxxxxxxxx (ip or hostname)
 * 
 * This program requires these jar files to compile and run
 * 
 * magnum.jar copied as part of the build facility
 * gson.jar copied as part of the build facilty
 * 
 * refer to https://gitlab.com/Magnum_Energy/distribution/blob/master/PLUGIN_BUILD.md
 *
 *  https://github.com/steveohara/j2mod#releases and follow the links and download the jar file
 *
 * j2mod-2.5.3.jar or newer
 *  
 *  https://www.slf4j.org/download.html
 *  The jar files need to be extracted from larger zip file
 * 
 *  	slf4j-api-1.6.0.jar
 *  	slf4j-simple-1.6.0.jar
 *  
 * This can be tested against a dummy slave
 * 
 * https://www.modbusdriver.com/diagslave.html
 *  
 */
import java.net.InetAddress;
import java.util.LinkedHashMap;
import java.util.Properties;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.io.ModbusTCPTransaction;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersRequest;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersResponse;
import com.ghgande.j2mod.modbus.net.TCPMasterConnection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import ca.godwin.magnum.DataReader;
import ca.godwin.magnum.ServerProperties;
import ca.godwin.magnum.TraceLogger;


public class MS_Reader implements DataReader {
	public static void main(String[] args) {
		ServerProperties.addOptions(args);
		try {
			DataReader reader = new MS_Reader();
			LinkedHashMap<String, Object> map = reader.getData();
			if (map.size() > 0) {
				Gson gson = new GsonBuilder().serializeNulls().create();
				String json = gson.toJson(map, LinkedHashMap.class);
				System.out.println(json);
			} else {
				System.out.println("No data");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	protected final Properties options = ServerProperties.getOptions();
	private float aScaling = -1;
	private float vScaling = -1; 
	protected boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	TCPMasterConnection con = null;
	public String MS_revision;
	private boolean failed = false;
	protected int MS_t_hs;
	protected int MS_t_rts;
	protected int MS_t_batt;
	protected float MS_adc_vb_f_1m;
	protected float MS_adc_ib_f_1m;
	protected int MS_fault_all;
	protected int MS_alarm;
	protected int MS_charge_state;
	protected String MS_charge_state_text;
	protected float MS_vb_ref;
	protected int MS_out_power;
	protected int MS_in_power;
	protected int MS_sweep_pin_max;
	protected float MS_vb_max_daily;
	protected float MS_va_max_daily;
	protected int MS_ahc_daily;
	protected int MS_whc_daily;
	protected int MS_flags_daily;
	protected int MS_tb_min_daily;
	protected int MS_pout_max_daily;
	protected int MS_tb_max_daily;
	protected int MS_fault_daily;
	protected int MS_alarm_daily;
	protected int MS_time_ab_daily;
	protected int MS_time_eq_daily;
	protected int MS_time_fl_daily;
	protected int MS_ekwhc_t = 0;
	protected float MS_vb_min_daily;
	// added 2018/12/30
	protected float MS_adc_ia_f_shadow;
	protected float MS_adc_ib_f_shadow;
	protected float MS_adc_va_f;
	protected float MS_adc_vb_f_med;
	protected float MS_adc_vbs_f;

	protected float MS_adc_vbterm_f;

	public MS_Reader() throws Exception {
		super();
		String address = options.getProperty("morningstar_address", "localhost");
		if (trace) {
			TraceLogger.logit("ModBus connection at:" + address);
		}
		final InetAddress addr = InetAddress.getByName(address);
		con = new TCPMasterConnection(addr);
		con.setPort(Modbus.DEFAULT_PORT);
		con.setTimeout(1000);
	}

	private float getAmp(int amp) {
		return amp * aScaling / 0x8000;
	}

	@Override
	public LinkedHashMap<String, Object> getData() throws Exception {
		return getMap();
	}

	private LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		try {
			getMSData();
			failed = false; // reset for next failure
		} catch (Exception e) {
			if (failed == false) {
				failed = true;
				TraceLogger.logit("Error detected reading MS Data:", e);
				// let one failure through. After that send empty map
			} else {
				return map;
			}
		}
		map.put("MS_revision", MS_revision);
		map.put("MS_adc_ib_f_1m", MS_adc_ib_f_1m);
		map.put("MS_adc_vb_f_1m", MS_adc_vb_f_1m);
		map.put("MS_ahc_daily", MS_ahc_daily);
		map.put("MS_alarm", MS_alarm);
		map.put("MS_alarm_daily", MS_alarm_daily);
		map.put("MS_charge_state", MS_charge_state);
		map.put("MS_charge_state_text", MS_charge_state_text);
		map.put("MS_ekwhc_t", MS_ekwhc_t);
		map.put("MS_fault_all", MS_fault_all);
		map.put("MS_fault_daily", MS_fault_daily);
		map.put("MS_flags_daily", MS_flags_daily);
		map.put("MS_in_power", MS_in_power);
		map.put("MS_out_power", MS_out_power);
		map.put("MS_pout_max_daily", MS_pout_max_daily);
		map.put("MS_sweep_pin_max", MS_sweep_pin_max);
		map.put("MS_tb_max_daily", MS_tb_max_daily);
		map.put("MS_tb_min_daily", MS_tb_min_daily);
		map.put("MS_time_ab_daily", MS_time_ab_daily);
		map.put("MS_time_eq_daily", MS_time_eq_daily);
		map.put("MS_time_fl_daily", MS_time_fl_daily);
		map.put("MS_t_batt", MS_t_batt);
		map.put("MS_t_hs", MS_t_hs);
		map.put("MS_t_rts", MS_t_rts);
		map.put("MS_va_max_daily", MS_va_max_daily);
		map.put("MS_vb_max_daily", MS_vb_max_daily);
		map.put("MS_vb_min_daily", MS_vb_min_daily);
		map.put("MS_vb_ref", MS_vb_ref);
		map.put("MS_whc_daily", MS_whc_daily);
		// added 2018/12/30
		map.put("MS_adc_ia_f_shadow", MS_adc_ia_f_shadow);
		map.put("MS_adc_ib_f_shadow", MS_adc_ib_f_shadow);
		map.put("MS_adc_va_f", MS_adc_va_f);
		map.put("MS_adc_vb_f_med", MS_adc_vb_f_med);
		map.put("MS_adc_vbs_f", MS_adc_vbs_f);
		map.put("MS_adc_vbterm_f", MS_adc_vbterm_f);
		return map;
	}

	private void getMSData() throws Exception {
		ModbusTCPTransaction trans = null;
		ReadInputRegistersRequest req = null;
		ReadInputRegistersResponse res = null;
		con.connect();
		req = new ReadInputRegistersRequest(0, 5);
		req.setUnitID(1);
		trans = new ModbusTCPTransaction(con);
		trans.setRequest(req);
		trans.setReconnecting(true);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		final int vpuhi = res.getRegisterValue(0);
		final int vpulo = res.getRegisterValue(1);
		vScaling = vpuhi + (float) (vpulo / 0x8000);
		final int apuhi = res.getRegisterValue(2);
		final int apulo = res.getRegisterValue(3);
		aScaling = apuhi + (float) (apulo / 0x8000);
		MS_revision = Integer.toString(res.getRegisterValue(4));
		// Added 2018/12/30
		req.setReference(0x18);
		req.setWordCount(6);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_adc_vb_f_med = getVolt(res.getRegisterValue(0));
		MS_adc_vbterm_f = getVolt(res.getRegisterValue(1));
		MS_adc_vbs_f = getVolt(res.getRegisterValue(2));
		MS_adc_va_f = getVolt(res.getRegisterValue(3));
		MS_adc_ib_f_shadow = getAmp(res.getRegisterValue(4));
		MS_adc_ia_f_shadow = getAmp(res.getRegisterValue(5));
		//
		req.setReference(0x23);
		req.setWordCount(5);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_t_hs = res.getRegisterValue(0);
		MS_t_rts = res.getRegisterValue(1);
		MS_t_batt = res.getRegisterValue(2);
		MS_adc_vb_f_1m = getVolt(res.getRegisterValue(3));
		MS_adc_ib_f_1m = getAmp(res.getRegisterValue(4));
		if (MS_adc_ib_f_1m < 0.0F || MS_adc_ib_f_1m > 100.0F) {
			MS_adc_ib_f_1m = 0;
		}
		req.setReference(0x2c);
		req.setWordCount(4);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_fault_all = res.getRegisterValue(0);
		MS_alarm = res.getRegisterValue(2) << 16;
		MS_alarm += res.getRegisterValue(3);
		req.setReference(0x32);
		req.setWordCount(2);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_charge_state = res.getRegisterValue(0);
		MS_vb_ref = getVolt(res.getRegisterValue(1));
		req.setReference(0x3a);
		req.setWordCount(3);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_out_power = (res.getRegisterValue(0) / 10);
		MS_in_power = (res.getRegisterValue(1) / 10);
		MS_sweep_pin_max = (res.getRegisterValue(2) / 10);
		req.setReference(0x40);
		req.setWordCount(16);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_vb_min_daily = getVolt(res.getRegisterValue(0));
		MS_vb_max_daily = getVolt(res.getRegisterValue(1));
		MS_va_max_daily = getVolt(res.getRegisterValue(2));
		MS_ahc_daily = res.getRegisterValue(3);
		MS_whc_daily = res.getRegisterValue(4);
		MS_flags_daily = res.getRegisterValue(5);
		MS_pout_max_daily = (res.getRegisterValue(6) / 10);
		MS_tb_min_daily = res.getRegisterValue(7);
		MS_tb_max_daily = res.getRegisterValue(8);
		MS_fault_daily = res.getRegisterValue(9);
		MS_alarm_daily = res.getRegisterValue(11) << 16;
		MS_alarm_daily += res.getRegisterValue(12);
		MS_time_ab_daily = res.getRegisterValue(13);
		MS_time_eq_daily = res.getRegisterValue(14);
		MS_time_fl_daily = res.getRegisterValue(15);
		req.setReference(0xE087);
		req.setWordCount(1);
		trans.execute();
		res = (ReadInputRegistersResponse) trans.getResponse();
		MS_ekwhc_t = res.getRegisterValue(0);
		setText();
	}

	private float getVolt(int volt) {
		return volt * vScaling / 0x8000;
	}

	@Override
	public void init() throws Exception {
	}


	private void setText() {
		switch (MS_charge_state) {
		case 0:
			MS_charge_state_text = "START";
			break;
		case 1:
			MS_charge_state_text = "NIGHT_CHECK";
			break;
		case 2:
			MS_charge_state_text = "DISCONNECT";
			break;
		case 3:
			MS_charge_state_text = "NIGHT";
			break;
		case 4:
			MS_charge_state_text = "FAULT";
			break;
		case 5:
			MS_charge_state_text = "MPPT";
			break;
		case 6:
			MS_charge_state_text = "ABSORPTION";
			break;
		case 7:
			MS_charge_state_text = "FLOAT";
			break;
		case 8:
			MS_charge_state_text = "EQUALIZE";
			break;
		case 9:
			MS_charge_state_text = "SLAVE";
			break;
		}
	}
}
