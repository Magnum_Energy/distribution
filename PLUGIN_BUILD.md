__How to build a plugin without building the entire Magnum Energy project__

This build process allows you to set up a directory and build any java plugin you want to add the the Magnum Server runtime.

Prerequisites are:

- A Java __JDK__ (not JRE) for Java 8 or better
  On a Raspberry Pi use  
  `sudo apt-get update`         
  `sudo apt-get install default-jdk`
- An installed ANT make program (<https://ant.apache.org/manual/install.html>)
  On a Raspberry Pi use  
  `sudo apt-get update`         
  `sudo apt-get install ant`

The *build.xml* file will build java plugins for use with the Magnum Energy data reader
To use:

- Create a new folder for your code
- Get the build.xml file using `wget -O build.xml https://gitlab.com/Magnum_Energy/distribution/raw/master/plugin_build.xml`
- Ensure you have ant and Java JDK installed (This is OS dependent)
- You must have an internet connection for the initial ant run as it installs jar files downloaded from the internet.
- Run the command `ant setup` to set up directories and prerequisites
- Edit the generated file ***build.properties*** to suit
- Insert your code in the /src directory
  If you want to test your build setup download src/WeatherReader.java from the repository and place it in *src* subdirectory and then run *ant*.
- Run `ant` and the resultant jar file will be in *build* subdirectory
- Repeat `ant` as needed to get error free compile
- When built, copy the jar file in *build* subdirectory to the lib subdirectory on the installed system for Magnum reader.
- To delete all built files and regenerate, run `ant clean`
- Refer to the DATASERVER.md document for information on modifying `conf/magnum-server.conf` to integrate your plugin into the server runtime
