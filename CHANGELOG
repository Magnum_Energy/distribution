Release 2.1.2 2020/06/11
       o Small changes for PT100 - NOTE: PT-100 data is incomplete
Release 2.1.1 2019/09/17
       o Fixed bug in log rotate files concerning defined user / group authorities
       o Fixed problem with some HAT type RS485 Serial devices producing only unknown packets
       o Revised Magnum reader to accumulate data over time as several packets are intermittent, 
         especially for the PT-100
       o Updated to jSerialComm 2.5.1 runtime library
       o Fixed bug in CSV data logging that prevented any data being written.
       o Fixed several data retrieval bugs in the AGS module
       o Fixed data bugs in java example for Morningstar controller
       o Fixed bug in event interval label for logged events "Min" changed to "Sec"
Release 2.1.0 2019/04/26
       o Added support for inline comments in filter files and some conf files
       o Added data type comment to generated filters
       o Server always return a numeric timezone offset. 'Z' is now +00:00
       o Corrected some spelling errors
       o Simplified use of database support in data logger. Refer to DATALOGGER.md for details.
       o Remove need for ./ prefix on all commands and scripts
       o Added support for multiple users running the software by revising access privileges
       o Now using version 2.4.2 of jSerialComm. To replace your version delete lib/jSerialComm.jar before updateing.
       
Release 2.0.2 2019/03/18
       o Added Access-Control-Allow-Origin", "*" to HTTP response header from server to support CORS
       o bug fixes on data type for some fields. These have been changed from numeric to boolean
         AGS_running, PT_on_off, PT_relay_state, PT_alarm_state
       o Fixed bug in disablelogger script
             
Release 2.0.1 2019/02/19
       o Fixed bug loading server drivers
       
Release 2.0.0 2019/01/25
       o Significant improvements to most logging code
       o Fixed bugs in building schema definition for FLOAT fields
       o Added Events generation
       o Refer to CHANGES_FOR_2.0.md for details
       o Refer to MIGRATING_TO_2.0.md for changes from prior version
       
Release 1.1.3 2018/01/12
       o Made internal changes to get ready for 2.0
       o Fixed bugs in writing generator log events to database
       o fixed bug with forcefilter option
       o Changed code to automatically create database and table, if needed, with first posting in logger
       o Support for the create option in buildschema has been removed and replaced with automatic create in the logger
       o !! Database support in loggenerator has changed!! Review DATALOGGER.md
       
Release 1.1.2 2018/12/15
       o Fixed bugs in start/stop/status scripts
       o Revised documentation to warn about needing sudo capability
       
Release 1.1.1 2018/12/14
       o Added update script to simplify update of software ( 1.1.x is a prerequisite)
       o Added support to write generator events to database(s)
       o Added hard coded directory settings
       o Removed complexity in logsyslog
       
Release 1.1.0 2018/12/02
       o Added capability to build plugins for the java runtime module without the need to rebuild the entire product.
       o Added -v (verbose) option for showlogger, showserver and showstatus
         This will provided a list of the last 50 lines from the event.
       o Added warnings about restarting programs during setup
       o Created a conf subdirectory to hold all configuration files.
           existing magnum_client.options file will be moved to conf subdirectory during install.
           existing magnum_server.options file will be moved to conf subdirectory during install.
       o Changed extension on SQLite data log files to _sqlite.sql from .lite.sql
       o Added support to optionally run the server and / or logger as systemd services. 
         This includes:
             o Uses runtime event logging to use rsyslog. 
             o The log files are rotated and compressed using the Linux utility logrotate. This can be user configured.
             o The data files are rotated and compressed using the Linux utility logrotate. This can be user configured.
         If this option is not used the programs run exactly as in previous releases

Release 1.0.7 2018/11/22
      o Cleaned up git errors
       
Release 1.0.6 2018/11/19
      o Fixed bug with TimeStamp and formatting Zone
      o Cleaned up Documentation typos
      o A new command ./version displays version of dataserver runtime

Release 1.0.5 2018/10/20
      o Revised architecture of datalogger
        - This will not impact current users
        - Allows support of extensions using plugins
      o Added a basic plugin to record generator events and save to a CSV file
      o Added full support for MySQL in datalogger
        - Log of INSERT SQL statement
        - Post of INSERT statement to MySQL server
      o Added full support for SQLite3 in datalogger
        - Log of INSERT SQL statement
        - Post of INSERT statement to SQLite datafile
      o Added ability to create schema in database
      o a New document title DATALOGGER.md has been created to better describe the data logging utility.
      o Magnum class has been deprecated (magnum.class.php) and replaced by Magnum2
        
If you want to use the data logger to post live data to SQL databases, please contact the author..
    
Release 1.0.4 2018/10/13
      o Significant changes to the repository. Now all the resources needed to build the software are available.
      o Revised install procedure to simplify internet install.
      o Double checked for good values for dataserver URL and port number
      o Added support for device name for the serial RS485 reader
      o Fixed bug when passing quoted values as run time parameter
      o The extension for the runtime logs for the server and the logger have been changed from .out to .log

Release 1.0.3 2018/10/07
     o Fixed bug in datalogger that was causing excesive header lines
     
Release 1.0.2 2018/10/04
    o Removed TestServer.java and DataClient.java as active modules
    o Removed need for in build project or runtime commons-csv-1.5.jar
    o Made client side programs more bulletproof
    o Reorganized documentation

Release 1.0.1 2018/10/02
    o Improved runtime activity logging
    o Fixed bug in server activity log date
    o Fixed bug processing PT-100 data
    o Enhanced traceing
    o Improved fault recovery
    o Fixed URLs in README
    
Release 1.0.0 2018/10/01
    o Implemented as simple REST webserver
    o Simplified plugins definition
