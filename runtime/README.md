__Runtime software__

Go to [this page](https://gitlab.com/Magnum_Energy/distribution/tags) for full description of each release
Refer to [GETTING_STARTED](.GETTING_STARTED.md) for detailed instructions.

__Installation__  
For the initial install, the computer where you are installing this software **MUST** be connected to the internet as it must download third party software that we use but is not distributed with the runtime files. Once this has run successfully you do not NEED to be connected to the internet for this software to run or to do future updates. It is highly recommended that you use internet based updates as they are much easier.

__Internet Install or update__

Create, if necessary, and change to, the folder where you want to install your software. and then run this command.  
`curl -sL http://bit.ly/magnumenergy |bash`

__Update without internet, restore older version or install pre-release version__

Download and copy the two files for the release you want into the install directory. 
 
If you wish to install older versions of the software, go to the [releases](https://gitlab.com/Magnum_Energy/distribution/releases) page for the version you want and download a zip of the entire project. The runtime directory will include the zip and setup files you need.