/* 
  This adds the new timestamp column 
 */
ALTER TABLE `magnum`.`log_data` 
ADD COLUMN `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `timezone`;
/* 
  this populates timestamp with correct timestamp using the old column values 
 */
update `magnum`.`log_data` set `timestamp` = timestamp(CONVERT_TZ(Date,case timezone when 'z' then '+0:00' else timezone end,@@global.time_zone));
/* 
  This drops old columns and resets the primary key to timestamp
 */
ALTER TABLE `magnum`.`log_data` 
DROP COLUMN `timezone`,
DROP COLUMN `Date`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`timestamp`);
