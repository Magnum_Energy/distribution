CREATE TABLE IF NOT EXISTS `events` (
            `ID` INT(11) NOT NULL AUTO_INCREMENT,
            timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `event_id` SMALLINT(6) NOT NULL DEFAULT 0,
            `description` VARCHAR(1024) NULL DEFAULT NULL,
            `metric_title` VARCHAR(50) NULL DEFAULT NULL,
            `metric_value` FLOAT NULL DEFAULT NULL,
            `metric_unit` VARCHAR(50) NULL DEFAULT NULL,
            `tags` VARCHAR(256) NULL DEFAULT NULL,
            `meta_data` VARCHAR(1024) NULL DEFAULT NULL,
            PRIMARY KEY (`ID`),
            INDEX `timestamp` (`timestamp`));
