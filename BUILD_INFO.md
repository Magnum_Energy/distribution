# Building the Magnum Energy Project

**You do NOT need to build this project**. There are prebuilt runtime versions available in the /runtime folder. use these instructions ONLY if you want to build the software from its original source.

If you want to build a plugin for the data server refer to PLUGIN_BUILD.md.

This project was developed using the Eclipse IDE (<https://eclipse.org>) and is capable of being built in a stand alone environment or as an Eclipse project.

Prerequisites are:

  - A Java __JDK__ (not JRE) for Java 8 or better
    On a Raspberry Pi use  
    `sudo apt-get update`         
    `sudo apt-get install default-jdk`

  - A standalone build environment using the ant make program or Eclipse
    Java IDE.

## Steps to build -- Standalone

  - Install the ANT make program
    (<https://ant.apache.org/manual/install.html>)
    On a Raspberry Pi use  
    `sudo apt-get update`         
    `sudo apt-get install ant`

  - Clone or download the entire project directory

  - Install the prerequisite software as needed

  - Run command `ant` and it will initialize and populate several
    directories and build a default system.

  - After this setup run, a configuration file is  generated named `build.properties`. This file is the override for all options set in the build.xml file used by ant for the build. 

  - Edit the file build.properties to set values here such as *builtby* and *release*. Set a valid release number. 

  - If you edited build.properties run ***ant clean build*** to build a version will the modified values
    The deliverables will be in a new sub-directory named *build*

## Steps to build -- Eclipse

If you are not an experienced user of Eclipse I recommend you use the
standalone method for building the project.

  - Use eGit to create a project from the GitHub site
  - Install the optional software if needed
  - You may get an automatic build, if not select the build.xml file, right click and select run as Ant Build
  - The first build will create and populate the lib folder and build these files.

      - build.properties  
        This file is the override for all options set in the build.xml
        file used by ant for the build.

      - php.ini  
        This file allows you to run php command line. Refer to the
        delivered documentation to use the correct command
  - Edit the file build.properties to set values here such as *builtby* and *release*. Set a valid release number. The value for release must NOT have any spaces. 
  - If you edited build.properties, Select Project -> Clean and Eclipse should rebuild the software.  
    The deliverables will be in a new folder named *build*
