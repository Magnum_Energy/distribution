**Why run Magnum Energy programs as a service**

If you want to run the software continuously so you can create a continuous log, then I recommend running the programs as a system service. The system will ensure the programs start when your machine reboots or if a program is terminated accidentally.

**How to run Magnum Energy as a Service**

This describes how to run the network server and / or the data logger as a service using systemd. This does not describe the features and use of systemd and systemctl nor give detailed instructions  in their use or the use of journald and journalctl as well as rsyslog and logrotate. These URLs can provide some interesting background material.

[Auto-Starting Programs on a Raspberry Pi](https://thepihut.com/blogs/raspberry-pi-tutorials/auto-starting-programs-on-the-raspberry-pi)

[Systemd on a Pi](https://www.raspberrypi.org/documentation/linux/usage/systemd.md)

[rsyslog](https://www.rsyslog.com/doc/v8-stable/index.html)

[logrotate](https://linux.die.net/man/8/logrotate)

This documentation and the provided scripts and configuration files require that these systems are installed and running on your machine. They are installed as part of a regular setup in current Raspberry Pi Raspbian installs.

- systemd
- rsyslogd
- logrotate is enable to run daily by cron program.

The installation script(`initialize`) prepares configuration files customized to your operating environment. They are placed in the *conf* subdirectory. Subsequent updates will not replace these files once they are created. They are named

* magnum-server.service
* magnum-server-syslog.conf
* magnum-server.rotate.conf
* magnum-logger.service
* magnum-logger-syslog.conf
* magnum-logger.rotate.conf

These are **NOT** automatically installed or enabled. you should review them and ensure they are as you want them. We do not recommend these programs are enabled as services until you are satisfied that your system is running in a stable state and all options have been chosen.

`enableserver / disableserver`enable and disable the server program as a service.

`enablelogger / disablelogger`enable and disable the logger program as a service.

All provided scripts to start, stop the programs and show their status are systemd aware and will check with systemd to see if the process is installed or operating as a service.

**Note**s: 

* If you make mistakes configuring these configuration files you may disable a critical service. for example an error in a syslog configuration file can disable **ALL** syslog facilities. ***Be careful, be very careful.***
* You **cannot** pass command line options to the programs if they are running as a service. All options must be defined in the .conf file before the service is started. If you change the options, the service must be restarted.
* You cannot setup and run a service from more than one directory at a time. There can only be one set of configuration settings for systemd and rsyslog. If you want to test a new directory then you must disable the service in the first directory before attempting to run in the second. The default start and stop scripts will detect an enabled service, regardless of its installed directory. ***Be careful, be very careful.***





