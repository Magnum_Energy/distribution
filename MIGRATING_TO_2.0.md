## Migrating to Release 2.0 of Magnum Energy Software

### Introduction

**If you are not using a version of the software prior to Release 2.0.0 you do not need to read this.**

It is <u>highly</u> recommended that you install Release 2 software in a new directory. All testing assumes a new directory for 2.0. 

Some things have changed, most things have not.

**NOTE**: Items marked with * prefix may impact advanced users or customizers. If you are just using the software as regular user, ignore these items.

### The Server

**The core code in the server, which reads the information packets from the Magnum Energy network, has not changed.**

#### Server Changes

- The current options file, named `conf/magnum_server.options` has been replaced by `conf/magnum-server.conf`. There are no tools provided to copy any custom settings you may have made in magnum_server.options and  to the new conf/magnum-server.conf file. An aid to identifying changes is to compare the default options file in the templates folder to the file in the conf folder. The Linux tool `diff` is useful for this.
- *The option `datetext` to rename the `Date` field name in  the generated JSON has been be deleted.
- The default log file folder is now named `log` instead of `logs`. Scripts have been revised to reflect this change.
- Before starting the new server do the following in the <u>old install directory</u>.
  - Stop the older version server `stopserver`
  - If you had the server running as a service then run the disable script `disableserver`.
- *The server had a function to store and retrieve key/value pairs. This feature has been removed.
- *If you added plugins please read the DATASERVER.md document as the convention for adding / disabling plugins has been simplified. You plugins will still work, it's just the values in the configuration file that have changed.

### The Logger

- The current options file, named `conf/magnum_client.options` has been replaced by `conf/magnum-logger.conf`. There are no tools provided to copy any custom settings you may have made in magnum_server.options and magnum_client.options to the new .conf files. An aid to identifying changes is to compare the default options file in the templates folder to the .conf file in the conf folder. The Linux tool `diff` is useful for this.
- Before starting the new data logger do the following in the <u>old install directory</u>.
  - Stop the older version logger. ./stoplogger
  - If you had the data logger running as a service then run the disable script`./disablelogger`.
- ***If you have been posting to MySQL contact me before migrating. There have been changes**!   I can be contacted at magnum@godwin.ca.
- *Class `Magnum2` has been replaced with class `Magnum` in all the php logger code.
- *Database name is fixed as `magnum` and table names are fixed as `log_data` and `events`.
- *The php.ini file is no longer referenced in standard program scripts.

