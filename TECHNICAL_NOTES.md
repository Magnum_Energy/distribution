[TOC]

# Introduction

When your server is up and running you can consider the following:

  - Customize a filter to extract only the data you want. See the filter section.

  - Customize the [runtime settings](#customizing-runtime)

  - Setup your platform to ensure the server and logger are always  running. 
      The recommended way is to start the programs as system services.
      Refer to the separate document named RUNNING_AS_SERVICE.md for details on how to make the processes run continuously. 
      **NOTE**: You cannot use command line override for options when using the service versions.

  - What are you going to do with the data?

      - Put it in a database?
        
      - Use spreadsheets?
        
      - Adjust the filter to provide less and reordered data?

  - Build a web interface to show online status and statistics.

      - Here the sky is the limit. 
        An interesting tool to consider is [Grafana](https://grafana.com/) to present everything as graphs.

# Customizing Runtime

Refer to the DATALOGGER and DATASERVER documents for customizing and runtime information.

# The Web Interface

The server is accessed via a web interface. The server has only one function, to return a JSON record based on current information on a Magnum Energy network and, optionally, any other plugin provided data.

That's all the server does. All other functionality such as CSV, SQL formatting, logging,  database updates and field filtering is performed by client side utilities.

These examples will assume the default server host name and port.

## Retrieve a JSON log record

The default web URL is http://localhost:19450/  
This will return a full JSON log record of all elements available in your network. A second endpoint /schema will return a JSON record with the similar to the regular data JSON but the values for every field will indicate the type of data for the field. This is useful for generating database table definitions.

There are no other supported endpoint, using any other endpoint will result in a 503 status code. Using a query string will also result in a 503 result.

## Helper Class Functions -- The API

There is a PHP class named Magnum ( magnum.class.php) that provides access to full services on the client side.

In all cases if $filter is a populated array it will be used to filter and order the returned result

**reset(string $json=null)**  
In order to allow various forms of data to all have the same time stamp the API only retrieves the data once from the server and all requests use the same data to provide results. Calling the Reset() function forces a flush and the next call will provide fresh data from the server. If a json string is passed it is used instead of a call to the server. This is used for simulations.

**getArray(array $filter=null)**  
Returns a keyed array of the data.

**getCSV(array $filter=null, $header=false)**  
Returns a CSV formatted line of values. If $header = true then a header line precedes the data line.

**

**getJson(array $filter=null)**  
Returns a JSON string, raw or filtered.

The PHP program datalogger.php is a good example of how to use the PHP class.

# Notes about permissions and PATH

## Permissions

During installation the group owner of the install directory is set to the group magnum. This group should also be the group owner on all files and sub directories.  You are also added to the system group `dialout`. n order to read the serial device you also must belong to the group `dialout`.

The software is installed so all files and directories have full read and write privileges for any member of a group named magnum. I If you want to add other users to this group so programs can be by other users, add the user to the group with this command.
`sudo usermod -a -G magnum <username>`
`sudo usermod -a -G dialout <username>`

User group memberships and PATH setting made during installation will only become active after you logout and then login again.

## PATH Setting

A script named `/etc/profile.d/magnum_path.sh` is executed automatically during the login process to set the PATH to the ../bin subdirectory of the install directory for all users in the magnum group.

# Problems, Questions or Comments

If you want to report a bug, ask a question or comment on the software, please create an issue by going to the project page on GitLab <https://gitlab.com/Magnum_Energy/distribution/issues> and add an issue. 
You must be a registered user of GitLab but this is simple and cost free process. 

Or write the author magnum@godwin.ca