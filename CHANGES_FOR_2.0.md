## Proposed Changes to Magnum Energy Software

##### Introduction

The entire project was tidied up. Most of the core executable code remains unchanged but the configuration files, used to set runtime choices, have been revised. The MySQL database have been improved. A new type of records, called and event, has been added. The documentation has been revised.

#### Server changes

- The server had function to support store and retrieve of text values. It has been deleted.
- The current options file, named `magnum_server.options` will be replaced with a new `magnum-server.con`f file with revised, simplified settings.
- The option `datetext` to rename the `Date` field name will be deleted.
- The operation of the server has not been changed.

#### Data Logger

- There is a new event reporting subsystem. This is an optional part of the datalogger and will generate records when an event occurs. An event is a report of a change of state. This release includes an event logger to create a record whenever the generator starts or stops. Data included is  a time stamp, event ID (needed for filtered retrieval), event description and a memo field containing SOC, Amp balance, battery voltage and, if available from an AGS unit, the reason the generator operated (automatic, manual, low-voltage, etc.). Event logging capability is extensible by use of PHP coded plugins.
- The current options file, named `magnum_client.options`, has been replaced with a new `magnum-logger.conf` file with revised, simplified settings. These setting will have less granularity. For example, if CSV output is selected it will be enabled for all data types (log and events). The intent of this new configuration file is to make it easier to define standard, out of the box, settings.
- __If you are logging to a MySQL database please contact me.__
- Users can able to generate files without an included date stamp in the filename. This allows data file logging to be easily supported by the Linux tool called [logrotate](https://www.networkworld.com/article/3218728/linux/how-log-rotation-works-with-logrotate.html).
- On the fly compression (gzip) in the datalogger has been deleted. 
- Support for SQLite will be dropped.
- The forcefilter option has been deleted and filters will always generate all fields defined in a filter
- The logger will provide, optionally, one or more of these actions.
  - For the data retrieved from the server:  Each action can have it's own filter to determine output.
    - CSV
    - JSON
    - MySQL INSERT into.... statements
    - Real-time MySQL database updates
  - For all events generated (events is optional). There is no filter capability for event records.
    - CSV
    - JSON
    - MySQL INSERT into.... statements
    - Real-time MySQL database updates
- Support for the current generator report, loggenerator, has been deleted. 
- The default log file directory is now named log for the runtime activity logs and log/data for generated data files such as JSON and CSV. The use of log instead of logs is to be consistent with Linux naming practices.

There is no formal support of this software or data for any third party analysis or graphing tools such as Grafana. 

