[TOC]

# Getting Started

This document references Raspberry Pi as the target platform. This software can also be used on other Linux / Intel systems as well as Mac OS or Windows. The author's experience is limited to the Pi and some Intel Linux so help with the other systems will be limited.

For help contact Charles Godwin email:magnum@godwin.ca

If you are new to Raspberry Pi or have never worked with it at the terminal or command line level, there is an excellent tutorial [here](https://www.raspberrypi.org/documentation/usage/terminal/).

Prerequisites: (You must have these software packages installed to successfully use this product)

  - Server platform has at least Java 8 (aka 1.8) installed  
    Type this at command line to verify  
    `java -version`

    If you get a problem then install Java runtime

    `sudo apt-get update`    
    `sudo apt-get install default-jre`

  - Client side platform has PHP 7.x installed  
    Type this at command line  
    `php --v`  
    If you get "bash: php: command not found." then you need to install php. Use these two commands: (You must be connected to the internet to install php)  
    `sudo apt-get update`   
    `sudo apt-get install php`
    If you plan to post directly to MySQL that tool must be installed. I won't describe that setup as there are many resources available. But be sure to add the php mysql components
    `sudo apt-get install php-mysql -y`

  - For the initial install, the computer where you are installing this software **MUST** be connected to the internet as it will download third party software that we can use but I do not distribute. Once this has run successfully you do not NEED to be connected to the internet for this software to run or to do future updates.

The initial steps are: (details follow)

  - Build / buy a RS485 serial to USB connector. There is a [separate document](Building_a_Magnum_Energy_Adaptor.pdf) describing how to make one of these.
  - Connect it to a USB port. The current software assumes USB but that can be overridden.
  - Install the software.
  - Run a program that will enumerate the serial devices known to the system and allow you to make any adjustments if needed.
  - Run a program that reads the serial device and ensures that the program can read proper data from the ME network.
  - Start the server.
  - Run a program that verifies the server is functioning properly.
  - Start the data logger.
  - Enjoy your data.

# Detailed Installation

The following covers the preceding points in detail.

### Build / buy a RS485 serial to USB connector

See separate document for information. This is needed to connect the ME network to your computer.

### Connect it to the intended machine USB port

This is just a plug in but shut down your computer before making the connection. Once connected restart the computer.

### Install software on the target machine

The first time you run this setup program the computer **MUST** be connected to the internet as it will download third party software that we use but I cannot distribute. Once this has run successfully you do not NEED to be connected to the internet for this software to run.

**NOTES**: 

The installation and execution of these programs should always be done with the same account to ensure there are no permissions issues. This user **must** have read and write privileges for the install folder and sub folders that may be created.

The install script as well as the start/stop scripts contain commands that must run with `sudo` super user permission. The account installing and using these scripts must have `sudo` capability. This is the default in Raspbian but other systems may have different standards.

  - On the computer that will be running the server software create a new directory with any name. I use /home/pi/magnum. This directory will be referred to as the ***install directory***.
  - [Go to this page to get install instructions for most recent version](https://gitlab.com/Magnum_Energy/distribution/releases). The release notes will probably say to execute one command that will both download and run the setup script.
     If you need to update the programs later, follow the steps for the latest release at that time.
  - **The first time you install software in a directory, or update from a version older than 2.1.0, you will get a message telling you that you must logoff and then login again. You must logout out after the install to ensure the PATH and access permissions for your account are set correctly. If you are using the graphic user interface (GUI) the simplest way may be to reboot the machine or log out as current user and login again. If you connected via SSH you will need to start a new connection.**
  - 
  [Refer to TECHNICAL_NOTES](./TECHNICAL_NOTES.md#notes-about-permissions-and-path) for more details about account setings.

### Run a program that will enumerate the serial devices 

Run this command to determine your USB serial interface  

`findserialport`

The output will look like this:

This is a list of ALL serial ports known to the serial driver software.

Index:0 device:/dev/ttyUSB0 Name:USB-to-Serial Port (ch341-uart)
Index:1 device:/dev/ttyAMA0 Name:Physical Port AMA0

On a Pi this will include /dev/ttyUSB0 or possibly /dev/ttyUSB1

If you get /dev/ttyUSB0, skip to next [topic](#run-a-test-program-that-reads-the-serial-device), you're done here.

For those who remained behind, I'm sorry, but this gets technical.

If none of your choices are /dev/ttyUSB0 refer to the section on customizing runtime in the [TECHNICAL_NOTES](TECHNICAL_NOTES.md) document to override the default datareaderdevice option. This override
is necessary to enable proper operation of the reader.

Change the default line from datareaderdevice=/dev/ttyUSB0 to datareaderdevice=/dev/xxxxxx, where this is the device identified.

**NOTE:** There are some systems where the device is non-standard or "roams" and changes address when the machine reboots. The solution is to this problem is to put the full title of the device, in this case
"USB-to-Serial Port (ch341-uart)", in magnum_server.options file.

`datareaderdevice="USB-to-Serial Port (ch341-uart)"`

The run time software will identify the correct device.

### Run a test program that reads the serial device 

Run this test program

`testrs485`

**NOTE:** The software reading the serial device should detect and recover from most failure situations such as no serial device or not connected to a ME network. But there are situations, as yet undiagnosed, where this test may go into an endless loop and never finish. If this happens, press \<Ctrl\>+C to end the program and contact the author.

This program will use the serial device just set up to read packets of data from the ME network. It should look like this:

> Bytes
> read:38:UNKNOWN=\>A102344D0084400001080004780001003319322A6B000001025800FF00003C04500F170601C8  
> Bytes read:18:BMK_81=\>815F0A3D003606CF0C33FFEB8B8900F70A01  
> Bytes read:21:INVERTER=\>400001080004780001003319322A6B000001025800  
> Bytes read:21:REMOTE_A3=\>00003C04500F170601C8A5860100581C0E24031EA3  
> Bytes read:21:INVERTER=\>400001080004780001003319322A6B000001025800  
> Bytes read:21:REMOTE_A4=\>00003C04500F170601C8A58601001E1400000000A4  
> Bytes read:6:AGS_A1=\>A102344D0083  
> Bytes read:21:INVERTER=\>400001080004780001003319322A6B000001025800  
> Bytes read:21:REMOTE_80=\>00003C04500F170601C8A5860100000000002B0080  
> Bytes read:21:INVERTER=\>400001080004780001003319322A6B000001025800  
> Bytes read:21:REMOTE_00=\>00003C04500F170601C8A586010000000000000000  
> Bytes read:21:INVERTER=\>400001080004780001003319322A6B000001025800  
> Bytes read:21:REMOTE_A0=\>00003C04500F170601C8A5860100102914006E01A0  
> Bytes read:6:AGS_A1=\>A102344F0085

If it does, then you can move on to next step. Otherwise....

If the display has lots of UNKNOWN packets, then the first troubleshoot idea is to switch the two wire connections on the USB adaptor. Be sure to turn off the computer before you do this. If that doesn't work, write Charles Godwin, <magnum@godwin.ca>.

# Start the server

Run this command

`startserver`

The server will start and stay running silently and the command prompt will return. The output, including error messages can be found in subdirectory named *log*

### Run a program that validates the server

There are two programs to run. The first, `showserver` just tells you the server is there and ready to work. The second one `testserver` is more serious and it connects to the server and displays a JSON dump of real live ME information.

# Start the data logger

The supplied data logging program is started with the command`startlogger`. Similar to the data
server this program runs silently in the background and error messages are in a file in the logs directory. This program, by default, writes a CSV entry to a log file in the log sub-directory every minute. The log
files are started fresh every day and their filename includes the date. You can change what format of logging you need by setting runtime options.

That's it.

If you want to setup the system so the server and or logger run as integrated system services, please refer to the `RUNNING_AS_SERVICE.md` file.
