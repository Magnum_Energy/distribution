# Changes to Magnum Energy Software  Release 2.1

**If you have not installed a version of this software older than 2.1.0 ignore this document.**

## Introduction

The major changes are:

- Revised install and directory structure
- Improved architecture for creating database specialized logging programs
- A new plugin for the data reader that supports non java programs for generating data

There is no need to upgrade at this time unless a new feature is needed. Please refer to GETTING_STARTED and TECHNICAL_NOTES.

## Revised Install

The install  has changed:

- A group named **magnum** is created
- The install directory, all installed files, and future files belong to group **magnum**
- The user installing/updating the software is added to the group **magnum** and, as before, group **dialout**
- The scripts have been moved to a ../bin sub directory. For older installed systems the scripts in the install directory will be removed.
- The PATH variable is modified during login to include the ../bin subdirectory so there is no need to use the ./ prefix for commands. The impact of this that you will have to relearn how to invoke the commands. ;)

## Improved architecture for database logging

The logging program for MySQL has been streamlined. It is easier to copy and revise for other database platforms. Refer to php/logdatasql.class.php and php/mysql.class.php  for an example of how its done. Now all the database specific code is the responsibility of the class making it easier to support varied database technologies.

## Support in data reader for non java programs

A new plugin has been created that allows running any command you can run at a command line to provide data to the server. This makes creating plugins for non-Java programmers significantly easier. For details see the DATASERVER.md document.