<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* 
 * This class is just used to define schemas
 */
 class MySQL  {
 	public function getSchemas(): array {
 		$schemas = array ();
 		$schema = $this->getLogSchema ();
 		if ($schema){
 		$schemas [] = $schema;
 		$schemas [] = $this->getEventsSchema ();
 		}
 		return $schemas;
 	}
 	public function getLogSchema() {
 		$schema = new Schema ();
 		$dofilter = filter_var ( @$options ["logger_mysql_filter"], FILTER_VALIDATE_BOOLEAN );
 		if ($dofilter) {
 			$filename = @$options ["logger_mysql_filtername"];
 			$filter = $schema->buildFilter ( $filename );
 		} else {
 			$filter = false;
 		}
 		$data = $schema->getArray ( $filter );
 		if ($data === false || count($data) == 0){
 			return "";
 		}
 		$sets = array ();
 		$schema = "CREATE TABLE IF NOT EXISTS `magnum`.`log_data`(\n    ";
 		$sets [] = "`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP";
 		foreach ( $data as $key => $obj ) {
 			if ($key != 'timestamp' && $key != "Date" && $key != "timezone") {
 				$field = "`$key`";
 				if ($obj != null) {
 					switch ($obj) {
 						case "Byte" :
 							$type = "TINYINT";
 							break;
 						case "Short" :
 							$type = "SMALLINT";
 							break;
 						case "Integer" :
 							$type = "INT";
 							break;
 						case "Long" :
 							$type = "BIGINT";
 							break;
 						case "Float" :
 							$type = "FLOAT";
 							break;
 						case "Boolean" :
 							$type = "TINYINT";
 							break;
 						case "String" :
 							$type = "VARCHAR(20)";
 							break;
 						default :
 							if (filter_var ( $obj, FILTER_VALIDATE_INT ) !== false) {
 								$type = "INTEGER";
 							} else if (filter_var ( $obj, FILTER_VALIDATE_FLOAT ) !== false) {
 								$type = "FLOAT";
 							} else if (filter_var ( $obj, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE ) !== NULL) {
 								$type = "BOOLEAN";
 							} else {
 								$type = "VARCHAR(20)";
 							}
 							break;
 					}
 					$sets [] = "$field $type DEFAULT NULL";
 				}
 			}
 		}
 		$sets [] = "PRIMARY KEY(`timestamp`)";
 		$schema .= implode ( "," . PHP_EOL . "    ", $sets );
 		$schema .= ');';
 		return $schema;
 	}
 	public function getEventsSchema() {
 		$schema = <<<EOD
CREATE TABLE IF NOT EXISTS magnum.events (
	ID INT NOT NULL AUTO_INCREMENT,
	timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	event_id SMALLINT(6) NOT NULL DEFAULT 0,
	description VARCHAR(1024) NULL DEFAULT NULL,
	metric_title VARCHAR(50) NULL DEFAULT NULL,
	metric_value FLOAT NULL DEFAULT NULL,
	metric_unit VARCHAR(50) NULL DEFAULT NULL,
	tags VARCHAR(255) NULL DEFAULT NULL,
	meta_data VARCHAR(1024) NULL DEFAULT NULL,
	PRIMARY KEY (ID),
    INDEX timestamp (timestamp));
EOD;
 		return $schema;
 	}
 	
}

