<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * This creates generator start stop events
 * It relies on INV_VACin and INV_chgled
 * If an AGS is attached it wikk use the AGS_status_text in description
 * generator start event id 10
 * generator stop  event id 11
 */
class GeneratorStatus extends MagnumEvent {
	public function initEvent(array &$checkpoint) {
		return;
	}
	public function checkChange(array &$checkpoint, array $data) {
		$timestamp = $data ["timestamp"];
		if (! isset ( $checkpoint ['running'] )) {
			$checkpoint ['running'] = 0;
		}
		if (! isset ( $checkpoint ['time'] )) {
			$checkpoint ['time'] = null;
		}
		if ($checkpoint ['time'] == null) {
			$checkpoint ['time'] = $timestamp;
		}
		$running = $data ["INV_VACin"] > 0.0 || $data ["INV_chgled"] == 1;
		if ($running != $checkpoint ['running']) {
			if (isset ( $data ['AGS_status_text'] ))
				$start = str_replace("Gen in ", "in ", $data ['AGS_status_text']);
			else
				$start = "Start";
			$eventdata = $this->getEventData ();
			$eventdata ['tags'] = "generator";
			$eventdata ['event_id'] = $running ? 10 : 11;
			$eventdata ['description'] = $running ? "Generator $start" : "Generator Stop";
			$eventdata ['metric_title'] = "Elapsed Time";
			$eventdata ['metric_unit'] = "Sec";
			$eventdata ['metric_value'] = $timestamp - $checkpoint ['time'];
			$meta = array();
			foreach (array("BMK_soc", "BMK_vdc", "BMK_amph") as $value){
				$meta[$value] = $data[$value];
			}
			$eventdata['meta_data'] = $meta;
			$checkpoint ['time'] = $timestamp;
			$checkpoint ['running'] = $running;
			
			$this->postData ( $eventdata );
		}
		return;
	}
}

