<?php

/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * This class is used to access the DataServer
 */

/*
 * Release 1.0.5 Derived from Magnum to support segregated filters
 */
class Magnum {
	protected $json = null;
	protected $unfiltered = array ();
	protected $baseURL;
	protected $statusURL;
	protected $fetchURL;
	protected $storeURL;
	protected $trace;
	protected $reset = true;
	protected $preprocesors;
	public function __construct($loadpreprocessors = true) {
		global $options;
		$host = "localhost";
		$port = 19450;
		if (@$options ["dataserver"])
			$host = $options ["dataserver"];
		$host = trim ( $host );
		if (! $host)
			$host = "localhost";
		if (@$options ["dataserverport"])
			$port = $options ["dataserverport"];
		$port = trim ( $port );
		if (! is_int ( $port )) {
			$port = 19450;
		}
		$this->trace = @$options ["trace"];
		$this->baseURL = sprintf ( "http://%s:%d/", $host, $port );
		$value = trim ( strtolower ( @$options ["magnum_preprocessors"] ) );
		if ($value && $loadpreprocessors) {
			$pluginlist = explode ( ",", $value );
			foreach ( $pluginlist as $item ) {
				$plugins [] = trim ( $item );
			}
			unset ( $item );
			$plugins = array_unique ( $plugins );
			$this->preprocesors = array ();
			foreach ( $plugins as $plugin ) {
				$driver = new $plugin ();
				if ($driver instanceof MagnumPreprocess) {
					if ($this->trace) {
						$class = get_class ( $driver );
						echo "$class loaded" . PHP_EOL;
					}
					$driver->init (  );
					$this->preprocesors [] = $driver;
				} else {
					echo "$plugin failed to load" . PHP_EOL;
				}
			}
		}
	}
	/*
	 * If this is called with a valid JSON sting, this will be loaded instead of forcing a read from the server
	 */
	public function reset($json = null) {
		$this->reset = true;
		$this->json = false;
		$this->unfiltered = array ();
		if ($json !== null)
			$this->loadJson ( $json );
	}

	/**
	 *
	 * @param $filename of filter definition
	 * @return associative array of field names = field title. Empty if no filter or no file
	 */
	public function buildFilter($filename) {
		$filter = array ();
		if (file_exists ( $filename )) {
			$file = new SplFileObject ( $filename );
			if ($file !== false) {
				while ( ! $file->eof () ) {
					$line = $file->fgets ();
					$ix = strpos($line, "#");
					if ($ix !== false){
						$line = substr ($line, 0, $ix);
					}
					$line = trim ( $line);
					if (strlen ( $line ) > 0) {
						$keys = explode ( ":", $line );
						for($keysix = 0; $keysix < count ( $keys ); $keysix ++) {
							$keys [$keysix] = trim ( $keys [$keysix] );
						}
						$key = $keys [0];
						$name = count ( $keys ) > 1 ? $keys [1] : $keys [0];
						$filter [$key] = $name;
					}
				}
				$file = null; // closes it
			}
		}
		return $filter;
	}

	/*
	 * Returns array of data fields
	 */
	protected function buildFiltered(Array $filter) {
		$unfiltered = $this->getArray ( false );
		if (count ( $filter ) == 0)
			return $unfiltered;
		$filtered = array ();
		$reserved = [ 
				"Date",
				"timezone"
		];
		foreach ( $reserved as $field ) {
			if (isset ( $unfiltered [$field] ))
				$filtered [$field] = $unfiltered [$field];
			else
				$filtered [$field] = null;
		}
		foreach ( $filter as $key => $title ) {
			if (isset ( $unfiltered [$key] )) {
				$filtered [$title] = $this->unfiltered [$key];
			} else {
				$filtered [$title] = NULL;
			}
		}
		return $filtered;
	}
	/*
	 * The string parameter allows loading from a file.
	 */
	protected function loadJson($json = null) {
		if ($this->reset || $json !== null) {
			$this->reset = false;
			if ($json === null) {
				$this->json = $this->getMessage ( $this->baseURL );
			} else {
				$this->json = $json;
			}
			if ($this->json === false) {
				$this->unfiltered = array ();
			} else {
				$this->unfiltered = json_decode ( $this->json, true );
				if (count ( $this->preprocesors ) > 0) {
					$data = $this->unfiltered;
					foreach ( $this->preprocesors as $processor ) {
						$data = $processor->process ( $data );
					}
					$this->unfiltered = $data;
					$this->json = json_encode ( $this->unfiltered, JSON_PRESERVE_ZERO_FRACTION || JSON_NUMERIC_CHECK );
				}
			}
		}
	}
	public function getJson($filter = null) {
		$this->loadJson ();
		if ($this->json === false)
			return false;
		if ($filter == null || count ( $filter ) == 0)
			return $this->json . PHP_EOL;
		return json_encode ( $this->getArray ( $filter ), JSON_PRESERVE_ZERO_FRACTION || JSON_NUMERIC_CHECK ) . PHP_EOL;
	}
	public function getArray($filter = null) {
		$this->loadJson ();
		if ($this->json === false)
			return false;
		if ($filter == null || count ( $filter ) == 0)
			return $this->unfiltered;
		return $this->buildFiltered ( $filter );
	}
	public function getCSV($filter = null, $header = false) {
		$this->loadJson ();
		if ($this->json === false)
			return false;
		$data = $this->getArray ( $filter );
		if (count ( $data ) == 0)
			return "";
		$handle = fopen ( 'php://temp', 'r+' );
		if ($header) {
			fputcsv ( $handle, array_keys ( $data ), ',', '"' );
		}
		fputcsv ( $handle, array_values ( $data ), ',', '"' );
		rewind($handle);
		$contents = '';
		while ( ! feof ( $handle ) ) {
			$contents .= fread ( $handle, 8192 );
		}
		fclose ( $handle );
		return $contents;
	}
	protected function getMessage($msg) {
		try {
			return file_get_contents ( $msg );
		} catch ( Exception $e ) { // server is down or bad
			trigger_error ( $e->getMessage () );
			return false;
		}
	}
}
