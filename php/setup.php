<?php
/*
 * This is run before all PHP files
 * It merges the contents the command line a=b variables into a global variable named $options
 * the command line values have presidence
 */
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
error_reporting ( E_ALL );
define ( "DATE", "Y-m-d" );  //used for date stamped file names
global $options;
class Setup {
	 function setOptions() {
	 	global $argv;
		$args = array ();
		for($ix = 1; $ix < count ( $argv ); $ix ++) {
			$arg = $argv [$ix];
			$pos = strpos ( $arg, "#" );
			if ($pos !== false) {
				$arg = substr ( $arg, 0, $pos );
			}
			$arg = trim ( $arg );
			if ($arg) {
				$args [] = $arg;
			}
		}

		parse_str ( implode ( '&', $args ), $parms );
		if (! isset ( $parms ['systemd'] ))
			$parms ['systemd'] = filter_var ( getenv ( "systemd" ), FILTER_VALIDATE_BOOLEAN );
		if (isset ( $parms ['include_path'] ))
			set_include_path ( $parms ['include_path'] );
		$optionsfile = @$parms ['config'];
		if (! $optionsfile) {
			$optionsfile = "conf/magnum-logger.conf";
		}
		$optionstring = "";
		if ($optionsfile && file_exists ( $optionsfile )) {
			$contents = file ( $optionsfile, FILE_SKIP_EMPTY_LINES );
			foreach ( $contents as $line ) {
				$ix = strpos ( $line, "#" );
				if ($ix !== false) {
					$line = substr ( $line, 0, $ix );
				}
				$line = trim ( $line );
				if ($line)
					$optionstring .= $line . PHP_EOL;
			}
		}
		foreach ( $parms as $key => $value ) { // add command line at end
			$optionstring .= $key . "=" . $value . PHP_EOL;
		}
		global $options;
		$options = parse_ini_string ( $optionstring, false, INI_SCANNER_TYPED );
		$optionlist = array (
				"systemd",
				"events_csv",
				"events_json",
				"logger_csv",
				"logger_csv_filter",
				"logger_json",
				"logger_json_filter",
				"logger_mysql_filter",
				"logger_mysql_log",
				"logger_mysql_post",
				"logger_nodate",
				"trace"
		);
		foreach ( ( array ) $optionlist as $item ) {
			if (isset ( $options [$item] ))
				$options [$item] = filter_var ( $options [$item], FILTER_VALIDATE_BOOLEAN );
			else
				$options [$item] = false;
		}
		if ($options ['systemd']) {
			$options ['logger_nodate'] = true;
		}
		unset ( $options ['systemd'] );
		if (! @$options ["logger_datadir"]) {
			$options ["logger_datadir"] = "log/data";
		}
		unset ( $optionstring );
		unset ( $optionsfile );
		if ($options ['trace']) {
			ini_set ( 'display_errors', '1' );
			echo "options" . PHP_EOL;
			foreach ( $options as $option => $setting ) {
				echo "   $option => $setting" . PHP_EOL;
			}
		}
		$_REQUEST = array_merge ( $_REQUEST, $options );
	}
}
$setup = new Setup();
$setup->setOptions ();
unset ($setup);
spl_autoload_register ( function ($class_name) {
	$name = strtolower ( $class_name );
	$path = stream_resolve_include_path ( "$name.class.php" );
	if ($path !== false) {
		include $path;
	} else {
		$path = stream_resolve_include_path ( "$name.interface.php" );
		if ($path !== false) {
			include $path;
		}
	}
} );
