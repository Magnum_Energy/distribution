<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
class LogDataSQL implements MagnumDBLog {
	private $filter;
	private $post;
	private $log;
	private $trace;
	private $logspath;
	private $events;
	private $logger_nodate;
	private $initialized = false;
	private $password;
	private $user;
	private $port;
	private $host;
	private $noscheama = false;
	private $mysqli = null;
	public function init() {
		global $server;
		global $options;
		$this->logger_nodate = filter_var ( @$options ["logger_nodate"], FILTER_VALIDATE_BOOLEAN );
		$this->trace = @$options ["trace"];
		$this->logspath = $options ["logger_datadir"];
		$this->post = filter_var ( @$options ["logger_mysql_post"], FILTER_VALIDATE_BOOLEAN );
		$this->log = filter_var ( @$options ["logger_mysql_log"], FILTER_VALIDATE_BOOLEAN );
		$this->noschema = filter_var ( @$options ["logger_mysql_noschema"], FILTER_VALIDATE_BOOLEAN );
		$dofilter = filter_var ( @$options ["logger_mysql_filter"], FILTER_VALIDATE_BOOLEAN );
		if ($dofilter) {
			$filename = @$options ["logger_mysql_filtername"];
			$this->filter = $server->buildFilter ( $filename );
		} else {
			$this->filter = false;
		}
		$this->events = (@$options ['events']) ? true : false;
		// mysql setup
		$this->user = @$options ["mysql_user"];
		$this->password = @$options ["mysql_password"];
		$this->port = @$options ["mysql_port"];
		if (! is_int ( $this->port )) {
			$this->port = 3306;
		}
		$this->host = @$options ["mysql_host"];
		if (empty ( $this->host ))
			$this->host = "localhost";
	}
	public function log() {
		global $server;
		$data = $server->getArray ( $this->filter );
		/*
		 * MySQL MUST have timestamp
		 */
		if (! isset ( $data ['timestamp'] )) {
			// cleanup missing timestamp
			$tzstring = @$data ['timezone'];
			$tz = new DateTimeZone ( $tzstring ? $tzstring : "+00:00" );
			$dtstring = @$data ['Date'];
			$timestamp = $dtstring?DateTime::createFromFormat ( "Y-m-d G:i:s", $data ['Date'], $tz ):new DateTime();
			// $timestamp = DateTime::createFromFormat ( "Y-m-d G:i:s", $data ['Date'], $tz );
			$data ['timestamp'] = $timestamp->getTimestamp ();
		}
		$sqlinsert = $this->buildInsert ( $data, "log_data" );
		if ($sqlinsert === false) {
			echo "Build insert failed" . PHP_EOL;
			print_r ( $data );
		} else {
			$this->doSQL ( $sqlinsert );
		}
	}
	public function postEvent(array $eventdata) {
		$insert = $this->buildInsert ( $eventdata , "events");
		$this->doSQL ( $insert );
	}
	public function connect(): bool {
		if ($this->post) {
			if ($this->mysqli == null) {
				$this->mysqli = new mysqli ( $this->host . ":" . $this->port, $this->user, $this->password );
				if ($this->mysqli->connect_errno) {
					$this->mysqli = null;
					trigger_error ( 'Connect Error (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error );
					return false;
				}
			}
			if (! $this->initialized && $this->noschema != true) {
				$mysql = new MySQL();
				$create = "CREATE SCHEMA IF NOT EXISTS `magnum`;";
				$result = $this->doSQL ( $create );
				if ($result === false) {
					trigger_error ( $mysqli->error );
					$this->mysqli = null;
					return false;
				}
				$create = $mysql->getLogSchema ();
				$create = trim ( preg_replace ( '/\s+/', ' ', $create ) );
				$result = $this->doSQL ( $create );
				if ($result === false) {
					trigger_error ( $mysqli->error );
					$this->mysqli = null;
					return false;
				}
				if ($this->events) {
					$create = $mysql->getEventsSchema ();
					$create = trim ( preg_replace ( '/\s+/', ' ', $create ) );
					$result = $this->doSQL ( $create );
					if ($result === false) {
						trigger_error ( $mysqli->error );
						$this->mysqli = null;
						return false;
					}
				}
				$this->initialized = true;
			}
		}
		return true;
	}
	public function disconnect() {
		if ($this->mysqli != null) {
			$this->mysqli->commit ();
			$this->mysqli->kill ( $this->mysqli->thread_id );
			$this->mysqli->close ();
			$this->mysqli = null;
		}
	}

	private function doSQL(String $command) {
		if ($this->log) {
			if ($this->logger_nodate) {
				$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_data.sql";
			} else {
				/*
				 * New log file is created every day
				 */
				$date = date ( DATE );
				$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_data_{$date}.sql";
			}
			file_put_contents ( $logname, $command . PHP_EOL, FILE_APPEND | LOCK_EX );
		}
		$return = false;
		if ($this->trace)
			echo $command . PHP_EOL;
		if ($this->post && $this->mysqli !== null) {
			$response = $this->mysqli->query ( $command, MYSQLI_USE_RESULT );
			if ($response === false) {
				$errtext = $this->mysqli->error;
				trigger_error ( $errtext );
				$return = false;
			} elseif ($response === true) {
				$return = true;
			} else {
				$return = $response->fetch_all ( MYSQLI_ASSOC );
				$response->free ();
			}
		}
		return $return;
	}
	private function buildInsert(array $data, string $table = "log_data") {
		$columns = array ();
		$values = array ();
		$mysql = "INSERT INTO `magnum`.`$table` ";
		if (isset ( $data ['timestamp'] )) {
			$timestamp = $data ['timestamp'];
		} elseif (isset ( $data ['timezone'] ) && isset ( $data ['Date'] )) {
			$tz = new DateTimeZone ( $data ['timezone'] );
			$timestamp = DateTime::createFromFormat ( "Y-m-d G:i:s", $data ['Date'], $tz )->getTimestamp ();
		} else {
			return false;
		}
		$columns [] = "`timestamp`";
		$values [] = "FROM_UNIXTIME($timestamp)";
		foreach ( $data as $key => $obj ) {
			if ($key != 'timestamp' && $key != "Date" && $key != "timezone") {
				$columns [] = "$key";
				if (is_array ( $obj )) {
					$value = json_encode ( $obj, JSON_PRESERVE_ZERO_FRACTION || JSON_NUMERIC_CHECK );
					$values [] = var_export ( $value, true );
				} else {
					if (is_null ( $obj )) {
						$value = "NULL";
					} elseif (filter_var ( $obj, FILTER_VALIDATE_INT ) !== false) {
						$value = $obj;
					} elseif (filter_var ( $obj, FILTER_VALIDATE_FLOAT ) !== false) {
						$value = $obj;
					} elseif (filter_var ( $obj, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE ) !== NULL) {
						$value = filter_var ( $obj, FILTER_VALIDATE_BOOLEAN ) ? 1 : 0;
					} elseif (is_array ( $obj )) {
						$value = '"' . str_replace ( '"', '\"', json_encode ( $obj, JSON_PRESERVE_ZERO_FRACTION || JSON_NUMERIC_CHECK ) ) . '"';
					} else {
						$value = '"' . str_replace ( '"', '\"', $obj ) . '"';
					}
					$values [] = $value;
				}
			}
		}
		$mysql .= "(" . implode ( ",", $columns ) . ") VALUES (" . implode ( ",", $values ) . ");";
		return $mysql;
	}
}
