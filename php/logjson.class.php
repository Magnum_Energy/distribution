<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
class LogJSON implements MagnumLog {
	protected $trace;
	protected $server;
	protected $filter;
	protected $logspath;
	protected $extension;
	protected $logger_nodate;
	public function __construct() {
	}
	public function init() {
		global $server;
		global $options;
		$this->trace = @$options ["trace"];
		$this->server = $server;
		$this->logspath = $options ["logger_datadir"];
		$this->logger_nodate = filter_var ( @$options ["logger_nodate"], FILTER_VALIDATE_BOOLEAN );
		$dofilter = filter_var ( @$options ["logger_json_filter"], FILTER_VALIDATE_BOOLEAN );
		if ($dofilter) {
			$filename = @$options ["logger_json_filtername"];
			$this->filter = $this->server->buildFilter ( $filename );
		} else {
			$this->filter = array ();
		}
		$this->extension = ".json";
	}
	public function log() {
		$json = $this->server->getJson ( $this->filter );
		if ($json) {
			if ($this->logger_nodate) {
				$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_data$this->extension";
			} else {
				/*
				 * New log file is created every day
				 */
				$date = date ( DATE );
				$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_data_{$date}$this->extension";
			}
			if ($this->trace)
				echo $json . PHP_EOL;
			file_put_contents ( $logname, $json, FILE_APPEND | LOCK_EX );
		}
	}
}
?>