<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * This class logs events
 *
 */
class LogEvents implements MagnumLog {
	protected $trace;
	private $checkpoints = null;
	private $checkpointsserial = null;
	private $serialpath;
	private $csv;
	private $json;
	private $data;
	private $logger_nodate;
	public function init() {

		global $options;
		$class = strtolower ( get_class ( $this ) );
		$this->csv = $options ["events_csv"];
		$this->json = $options ["events_json"];
		$this->trace = $options ["trace"];
		$this->logger_nodate = $options ['logger_nodate'];
	
		$class = strtolower ( get_class ( $this ) );
		$this->serialpath = "log";
		if (! file_exists ( $this->serialpath )) {
			mkdir ( $this->serialpath );
		}
		$this->checkpointserial = $this->serialpath . DIRECTORY_SEPARATOR . "logevents.serial";
		$this->logspath = $options ["logger_datadir"];
		/*
		 * end of common settings
		 */
		if (file_exists ( $this->checkpointserial )) {
			$string = file_get_contents ( $this->checkpointserial );
			$this->checkpoints = unserialize ( $string );
		}
		$value = trim ( strtolower ( @$options ['events'] ) );
		$events = array ();
		if ($value) {
			$eventslist = explode ( ",", $value );
			foreach ( $eventslist as $item ) {
				$events [] = trim ( $item );
			}
			$events = array_unique ( $events );
			$callback = array (
					$this,
					"postData"
			);
			$this->events = array ();
			foreach ( $events as $event ) {
				$this->events [$event] = new $event ( $callback );
				if (! isset ( $this->checkpoints [$event] )) {
					$this->checkpoints [$event] = array ();
				}
				$checkpoint = $this->checkpoints [$event];
				$this->events [$event]->initEvent ( $checkpoint );
				$this->checkpoints [$event] = $checkpoint;
			}
			$this->archiveCheckpoints ();
		}
	}
	public function log() {
		$before = $string = serialize ( $this->checkpoints );
		global $server;
		$this->data = $server->getArray ( array () );
		/*
		 * There MUST e atimezone record
		 */
		if (! isset ( $this->data ['timestamp'] )) {
			// cleanup missing timestamp
			$tz = new DateTimeZone ( $this->data ['timezone'] );
			$timestamp = DateTime::createFromFormat ( "Y-m-d G:i:s", $this->data ['Date'], $tz );
			$this->data ['timestamp'] = $timestamp->getTimestamp ();
		}
	foreach ( $this->events as $eventname => $event ) {
			$checkpoint = $this->checkpoints [$eventname];
			$event->checkChange ( $checkpoint, $this->data );
			$this->checkpoints [$eventname] = $checkpoint;
		}
		$after = serialize ( $this->checkpoints );
		if ($before !== $after) {
			$this->archiveCheckpoints ();
		}
	}
	/*
	 * routine will fill in the three key fields
	 */
	public function postData(array $eventdata) {
		if ($this->logger_nodate) {
			$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_events";
		} else {
			/*
			 * New log file is created every day
			 */
			$date = date ( DATE );
			$logname = $this->logspath . DIRECTORY_SEPARATOR . "magnum_events_{$date}";
		}
		$eventdata ["timestamp"] = $this->data ["timestamp"];
		global $logdrivers;
		foreach ( $logdrivers as $logdriver ) {
			if ($logdriver instanceof MagnumDBLog) {
				$logdriver->postEvent ( $eventdata );
			}
		}
		
		/*
		 * fix up for logging
		 */
		$newarray = array ();
		$newarray ["Date"] = $this->data ['Date'];
		$newarray ["timezone"] = $this->data ['timezone'];
		//$newarray ['timestamp'] = $timestamp->getTimestamp ();
		$eventdata = array_merge ( $newarray, $eventdata );
		unset($eventdata["timestamp"]);
		if ($this->csv) {
			if (! file_exists ( $logname . ".csv" )) {
				$result = $this->toCSV ( $eventdata, true );
			} else {
				$result = $this->toCSV ( $eventdata, false );
			}
			file_put_contents ( $logname . ".csv", $result, FILE_APPEND | LOCK_EX );
		}
		if ($this->json) {
			$json = json_encode ( $eventdata, JSON_PRESERVE_ZERO_FRACTION || JSON_NUMERIC_CHECK ) . PHP_EOL;
			file_put_contents ( $logname . ".json", $json, FILE_APPEND | LOCK_EX );
		}
	}
	private function toCSV(array $data, $header = false) {
		$handle = fopen ( 'php://temp', 'r+' );
		if ($header) {
			fputcsv ( $handle, array_keys ( $data ), ',', '"' );
		}
		fputcsv ( $handle, array_values ( $data ), ',', '"' );
		rewind ( $handle );
		$contents = '';
		while ( ! feof ( $handle ) ) {
			$contents .= fread ( $handle, 8192 );
		}
		fclose ( $handle );
		return $contents;
	}
	protected function archiveCheckpoints() {
		$string = serialize ( $this->checkpoints );
		file_put_contents ( $this->checkpointserial, $string );
	}
}

