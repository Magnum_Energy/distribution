<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * This generates sunrise and sunset events based on date_sunrise and date_sunset functions
 * and reliable latitude and longitude values
 * There is no default location
 * 
 * Event ID 
 *    20 - sunrise
 *    21 - sunset
 *
 */
class Daylight extends MagnumEvent {
	protected $lat;
	protected $long;
	protected $ok = false;
	public function initEvent(array &$checkpoint) {
		global $options;
		if (isset($options['daylight_latitude'])){
			$this->lat = filter_var($options['daylight_latitude'], FILTER_VALIDATE_FLOAT);
		}
		if (isset($options['daylight_longitude'])){
			$this->long = filter_var($options['daylight_longitude'], FILTER_VALIDATE_FLOAT);
		}
		if (is_float($this->lat) && is_float($this->long))
			$this->ok = true;
		return;
	}
	public function checkChange(array &$checkpoint, array $data) {
		if (!$this->ok)
			return;
		$timestamp = $data ["timestamp"];
		$suninfo = date_sun_info ( $timestamp, $this->lat, $this->long);
		if (!$suninfo)
			return;
		$sunrise = $suninfo['sunrise'];
		$sunset = $suninfo['sunset'];
		if ($sunrise > $timestamp || $sunset < $timestamp)
			$daylight = 0;  // night
		else 
			$daylight = 1;  // day
		if (! isset ( $checkpoint ['daylight'] )) {
			$checkpoint ['daylight'] = $daylight;
		}
		if (! isset ( $checkpoint ['daylighttime'] )) {
			$checkpoint ['daylighttime'] = $timestamp;
		}
		if ($daylight != $checkpoint ['daylight']) {
			$eventdata = $this->getEventData();
			$eventdata ['tags'] = "daylight";
			$text = $daylight == 1? "Sunrise" : "Sunset";
			$eventdata ['event_id'] = $daylight == 1 ? 20 : 21;
			$eventdata ['description'] = $text;
			$eventdata ['metric_title'] = "Interval";
			$eventdata ['metric_unit'] = "Sec";
			$eventdata ['metric_value'] = $timestamp - $checkpoint ['daylighttime'];
			$checkpoint ['daylight'] = $daylight;
			$checkpoint ['daylighttime'] = $timestamp;
			$this->postData ( $eventdata );
		}
		return;
	}
}

