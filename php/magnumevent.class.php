<?php

/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * Used by all plugins used by logevents
 */
abstract class MagnumEvent {
	private $callback;
	public abstract function initEvent(array &$checkpoint);
	public abstract function checkChange(array &$checkpoint, array $data);
	public function __construct(array $callback) {
		$this->callback = $callback;
	}
	protected final function postData(array $eventdata) {
		call_user_func ( $this->callback, $eventdata );
	}
	/*
	 * Allways use this to initialize an event
	 * This is necesary so the data is in a predictable sequence.
	 */
	protected function getEventData() {
		return [ 
				"timestamp" => (int) 0, // must be integer
				"metric_title" => '',
				'metric_unit' => '',
				"tags" => '',
				"event_id" => ( int ) 0, // should be integer
				"description" => '',
				"metric_value" => ( float ) 0.0, // should be float
				"meta_data" => ''
		];
	}
}
?>