<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 * This uses option = schema class schema=<class name of databsse definer> default is mysql
 *
 */
require_once 'setup.php';
$builder = @$options['schema'];
if (!$builder)
	$builder = 'mysql';
$builder = strtolower($builder);
$schema = new $builder();
$schemas = $schema->getSchemas();
if (count($schemas) > 0){
	$output = "";
	foreach($schemas as $create){
		$output .= $create . PHP_EOL;
	}
	$logspath = $options ["logger_datadir"];
	$logname = $logspath . DIRECTORY_SEPARATOR . "$builder.sql";
	file_put_contents ( $logname, $output );
	echo "A schema definition has been placed in a file named ${logname}" . PHP_EOL;
} else {
	echo "No schema definition was avalable" . PHP_EOL;
}
