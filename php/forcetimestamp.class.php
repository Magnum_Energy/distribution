<?php

/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * Removes timestamp - NOT recomended
 */
class ForceTimestamp implements MagnumPreprocess {
	public function init() {
	}
	public function process(array $data) {
		if (! isset ( $data ['timestamp'] )) {
			// cleanup missing imestamp
			$tz = new DateTimeZone ( $data ['timezone'] );
			$timestamp = DateTime::createFromFormat ( "Y-m-d G:i:s", $data ['Date'], $tz );
			// puts everything in order
			$newarray = array ();
			$newarray ["Date"] = $data ['Date'];
			$newarray ["timezone"] = $data ['timezone'];
			$newarray ['timestamp'] = $timestamp->getTimestamp ();
			$data = array_merge ( $newarray, $data );
		}
		return $data;
	}
}
?>