<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * This is a data logger for the MagnumReader
 * It support logging in CSV, JSON
 * Database are supported for both logging insert commands or live inserts
 * Current database support is for MySQL
 * Logging interval can be from every 10 seconds to every hour
 */
require_once 'setup.php';
$server = new Magnum ();
$trace = @$options ["trace"];
$loginterval = @$options ["logger_interval"];
if (! $loginterval)
    $loginterval = @$options ["loginterval"];
if (! $loginterval)
	$loginterval = 60; // one minute
elseif ($loginterval > 360)
	$loginterval = 360;
elseif ($loginterval < 10)
	$loginterval = 10;
$logspath = trim ( @$options ["logger_datadir"] );
if (! $logspath) {
	$logspath = "log/data";
}
$logger_mode = strtolower ( @$options ["logger_mode"] );
if ($logger_mode == 'file') {
	$options['logger_mysql_noschema=true'] = true;
}
$options ["logger_datadir"] = $logspath;
if (! file_exists ( $logspath )) {
	mkdir ( $logspath );
}
$plugins = array ();
if ($options ['logger_csv'])
	$plugins [] = "logcsv";
if ($options ['logger_json'])
	$plugins [] = "logjson";
if ($options ['logger_mysql_log'])
	$plugins [] = "logdatasql";
if ($options ['logger_mysql_post'])
	$plugins [] = "logdatasql";
if (@$options ['events'])
	$plugins [] = "logevents";

$value = trim(strtolower ( @$options ["logger_plugins"] ));
if ($value) {
	$pluginlist = explode ( ",", $value );
	foreach ( $pluginlist as $item ) {
		$plugins [] = trim ( $item );
	}
}
unset ( $item );
$plugins = array_unique ( $plugins );
global $logdrivers;
$logdrivers = array ();
if (count ( $plugins ) == 0) {
	$plugins[] = "logcsv";
	$options ['logger_csv'] = true;
}
foreach ( $plugins as $plugin ) {
	$driver = new $plugin ();
	if ($driver instanceof MagnumLog) {
		if ($trace) {
			$class = get_class ( $driver );
			echo "$class loaded" . PHP_EOL;
		}
		$driver->init ();
		$logdrivers [] = $driver;
	} else {
		echo "$plugin failed to load" . PHP_EOL;
	}
}
if (count ( $logdrivers ) == 0) {
	die ( "No loggers defined" );
}
$reader = null;
if ($logger_mode == 'file') {
	$options['logger_mysql_noschema=true'] = true;
	$logger_file_filename = $options ["logger_file_filename"];
	$reader = gzopen ( $logger_file_filename, "r" );
	if ($reader === false) {
		echo " Error opening test data from:$logger_file_filename" . PHP_EOL;
		exit ( 1 );
	}
	$loops = $options ["logger_file_count"];
	if (!$loops)
		$loops = -1;
	echo "Reading file data from:$logger_file_filename" . PHP_EOL;
	while ( $loops-- != 0 ) {
		if (gzeof ( $reader ))
			break;
		$json = gzgets ( $reader, 4096 );
		if ($json === null) {
			break;
		}
		$server->reset ( $json );
		$json = $server->getJson ( false );
		foreach ($logdrivers as $logdriver){
			if ($logdriver instanceof MagnumDBLog){
				$logdriver->connect();
			}
		}
		foreach ( $logdrivers as $logdriver ) {
			$logdriver->log ();
		}
	}
	/*
	 * only end once
	 */
	foreach ($logdrivers as $logdriver){
		if ($logdriver instanceof MagnumDBLog){
			$logdriver->disconnect();
		}
	}	
	echo "Finished file data from:$logger_file_filename" . PHP_EOL;
	exit ();
}
$connected = false;
while ( true ) {
	/*
	 * It is very important to invoke reset() at the beginning of every loop
	 * This forces a new log record form the server
	 */
	$start = time ();
	if ($trace)
		echo "Starting event" . PHP_EOL;
	$json = null;
	$server->reset ( $json );
	$json = $server->getJson ( false );
	if ($json !== false) {
		if (! $connected) {
			$connected = true;
			echo "Connected to server" . PHP_EOL;
		}
		foreach ($logdrivers as $logdriver){
			if ($logdriver instanceof MagnumDBLog){
				$logdriver->connect();
			}
		}
		foreach ( $logdrivers as $logdriver ) {
			$logdriver->log ();
		}
		foreach ($logdrivers as $logdriver){
			if ($logdriver instanceof MagnumDBLog){
				$logdriver->disconnect();
			}
		}
	} else {
		$connected = false;
		echo "Error encountered:" . error_get_last () ["message"] . PHP_EOL;
	}
	$end = time ();
	$time = $loginterval - ($end - $start);
	if ($trace && $time > 0)
		echo "Sleeping for $time seconds" . PHP_EOL;
	if ($time > 0) {
		sleep ( $time ); // sleep for remainder of interval time
	}
}
