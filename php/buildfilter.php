<?php
/*
 * This software was developed by Charles Godwin magnum@godwin.ca
 *
 * Copyright (c) 2018
 * This file is part of ca.godwin.magnum.
 * ca.godwin.magnum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ca.godwin.magnum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ca.godwin.magnum. If not, see <http://www.gnu.org/licenses/>.
 *
 */
require_once 'setup.php';
global $options;
$server = new Schema ();
/*
 * This builds a full filter file based on what is found in your Magnum Energy system
 */
$data = $server->getArray (); // get unfiltered Array
if ($data === false) {
	echo "Error encountered:" . error_get_last () ["message"] . PHP_EOL;
	exit ();
}
if (count ( $data ) == 0) {
	echo "No result from server";
	exit ();
}
$reserved = [ 
		"Date",
		"timezone"
];
foreach ( $reserved as $field ) {
	unset ( $data [$field] );
}
$comments = array (
		"# ",
		"# All text on a line following # is ignored.",
		"# The name on left of the colon is internal name known to system, do NOT modify.",
		"# You can change the order of fields. They will be generated in the order in this file.",
		"#",
		"# The name on right of colon is column title or field name.",
		"# The first field in a record is always the date timestamp, followed by the timezone offset.",
		"# These fields are always the first two fields",
		"#",
		"# Filter files can be used by defining logger_xxxx_filterfile= in the conf/magnum-logger.conf file."
);
foreach ( $comments as $line )
	echo $line . PHP_EOL;
$max = 0;
foreach ( array_keys ( $data ) as $field ) {
	$max = max ( $max, strlen ( $field ) );
}
$group = "";
foreach ( $data  as $field => $type ) {
	$pos = strpos ( $field, "_" );
	if ($pos !== false) {
		$prefix = substr ( $field, 0, $pos );
		if ($prefix != $group) {
			echo "#" . PHP_EOL . "# Start of device $prefix" . PHP_EOL . "#" . PHP_EOL;
			$group = $prefix;
		}
	}
	$pad = str_pad ( $field, $max );
	echo "$pad : $pad # $type" . PHP_EOL;
}

?>