<?php
require_once 'setup.php';
$server = new Magnum();
/*
 * See magnum.class.php
 *
 * This is just an example of how to extract data from the reader server
 *
 */

$json = $server->getJSON(null); // get JSON base on value in $usefilter
if ($json === false) {
	echo  "Error encountered:" . error_get_last()["message"] . PHP_EOL;
    exit();
}
/*
echo $json;
$array = $server->getArray( null); // get same data as an array
if (count($array) == 0) {
    echo "No result from server";
    exit(0);
}
print_r($array);
*/
/*
 * Display  data as CSV
 */
echo $server->getCSV(null, true);
?>