[TOC]

# Introduction

This software includes a data logger implemented using PHP. It is intended to be a long running program to do the following: (see below for enabling or changing these settings)

  - Read data from the Magnum Energy (ME) data reader every 60 seconds
  - Create a log file in CSV format of all this data.
  - New log file is created daily, with date as part of name.
  - Write a JSON formatted file as well as, or instead of, the CSV file.
  - Support logging of events such as generator start and stop events.
  - Support live database logging of data and events to MySQL 
  - A filter can be applied to the data so that the log records contain only the required fields and in the order defined in the filter's definition.
  - Allow for custom plug-in processes for specialized logging.

# Using the Datalogger

The datalogger is started by running the command `startlogger`. The data logger does not have to run on the same machine as the ME data server but the system running the logger (AKA Client machine) must have PHP enabled.

If you are satisfied with the default logging behaviour you do not need to make any changes. Default behaviour is to write a record of all data points to a CSV file every 60 seconds and create a new date stamped log file every day.

The logger and other utilities uses a configuration (conf) file that can be be found in conf/magnum-logger.conf. Modify this file to suit your needs.

When running the command line version of the programs, you can also override all options at the command line by including the name=value parameter when invoking the command. If specifying an override value on the command line and there are spaces or special characters in the value, then surround the value with quotation marks. Like  `option="this value has space"`

| Option                            | Default   | Comment                                                      |
| --------------------------------- | --------- | ------------------------------------------------------------ |
| dataserver                        | localhost | Must be changed if you move access software to a different machine than server |
| dataserverport                    | 19450     | Port the server listens on. This must match the value set for the data server. |
| logger_interval                   | 60        | Seconds between logging events. Can be any value between 10 and 360 (1 hour). |
| logger_nodate                     | false     | true/false to enable/disable imbedding a date in the generated log file names. |
| logger_datadir                    | log/data  | the directory where all log files will be placed.            |
| logger_csv                        | true      | true/false to enable/disable CSV logging.                    |
| logger_csv_filter                 | false     | true/false to enable/disable CSV filtering. A logger_csv_filtername must be provided too |
| logger_csv_filtername             |           | Name of filter file name for CSV logging                     |
| logger_json                       | false     | true/false to enable/disable JSON logging.                   |
| logger_json_filter                | false     | rue/false to enable/disable JSON filtering. A logger_json_filtername must be provided too. |
| logger_json_filtername            |           | Name of filter file name for JSON logging                    |
| logger_mysql_log                  | false     | true/false to enable/disable MySQL INSERT into logging.      |
| logger_mysql_post                 | false     | true/false to enable/disable real time MySQL INSERT into database. |
| logger_mysql_filter               | false     | true/false to enable/disable filtering. A logger_musql_filtername must be provided too |
| logger_mysql_filtername           |           | Name of filter file name for MySQL logging                   |
| mysql_host                        | localhost | Host name of MySQL server                                    |
| mysql_port                        | 3306      | Port number of MySQL server                                  |
| mysql_user                        |           | User with full privileges on magnum database                 |
| mysql_password                    |           | Password of user                                             |
| events                            |           | name of events builders. Supplied event builders are `generatorstatus` and `daylight`. If no builders are included here, no events will be generated. |
| events_csv                        | false     | true/false to enable/disable CSV logging of events           |
| events_json                       | false     | true/false to enable/disable JSON logging of events          |
| daylight_latitude                 |           | latitude is north (+) and south (-) of the equator. Used by daylight event builder to calculate sunrise and sunset. |
| daylight_longitude                |           | longitude is east (+) and west (-) of the greenwich meridian. Used by daylight event builder to calculate sunrise and sunset. |
|                                                                          ||The following options are for advanced use only.|
| logger_plugins ||Custom loggers can be created and added to the processing in the datalogger. For an example of how to write one, refer to php/logcsv.class.php or other programs that implement the `MagnumLog` interface.|
| logger_mode                       | live      | Can be `live` to read the dataserver or `file` to read a JSON file of previously logged records. The logger will stop when the file has been read. |
| logger_file_filename              |           | The name of the file to read if logger_file_filename = true. |

## Filters

Filters allow you to define the content of the log record extracts in each of CSV, JSON or SQL formats. You can remove unwanted fields in your output data and you can reorder their occurrence. You can define different text for individual field names.

The software, as installed, has no filter provided except for two example filters.

You build your own site specific filters by running a program named `buildfilter`. **The data server must be running and available**. This generates a filter of all fields available for your site and your ME devices. The generated file is named log/sample.filter. Rename it to a name of your choice and edit it to remove any fields you don't want to retrieve. Once you have this done, change the various filterfile options in conf/magnum-logger.conf to indicate this file. You can have separate and different filters for your CSV logging, JSON logging and MySQL processing.

When you have created your filter files and update the conf file, restart your logger and you will now have your custom choice of fields. 

There are many fields, particularly if you have a remote control (prefix ARC\_), that aren't too meaningful for usage statistics.

The data server only generates log records with fields for operating components. For example, if no AGS device is detected, the AGS fields and relate Remote Control (ARC\_) fields will not be generated.

 There is an example filter supplied named `magweb.filter`. It demonstrates how column names can be changed as well as the order. The columns selected are meant to represent, as well as possible, the data extracted by the MagWeb interface. The MagWeb website does some statistical analysis, such as MIN, MAX and AVG which is beyond the scope of this project. Also include is a file named `allfields.filter` which is really a document explaining what fields relate to which components in a ME system.

## Logging Events

The data logger can also generate events to one or more of CSV, JSON or SQL INSERT records, and posts to a MySQL database. Events recorded include generator stop and start events as well as sunrise and sunset.  It is enabled by defining a event logger in the events= option of the conf file. The current event loggers are

- `generatorstatus` that logs generator start and stop events.
  The generator event logger requires a minimum of an inverter and Battery Monitor Kit (BMK) installed. If an Automatic Generator Start - Network (AGS) unit is installed it will report the AGS status and text. This event logger should not be used in a system with regular mains AC input as it is  attempting to record generator activity. 
- `daylight` which logs sunrise and sunset for a defined location. The latitude and longitude of the location must be supplied in the config file.

This feature can be extended with custom code. Refer to `php/daylight.class.php` for an example. All extensions must extend the MagnumEvent class. To use them add them to the list of names in the events= setting in the conf file.

## Databases and the logger

Before attempting to use the database you must ensure the appropriate software is installed on your client computer. Neither is installed by default on Linux or Windows systems. You will need both the base MySQL (or MariaDB) software as well as a PHP extension. These vary between versions of operating systems and the setup of these services is beyond the scope of this document.

Only MySQL(MariaDB) is supported.

  - Generate a filter specific CREATE TABLE statement

  - The ability to programmatically initialize the table in the database

  - Log of INSERT INTO table SQL statements

  - Execution, in real time, of the INSERT statement into the database

### Initializing Database Schema

In order for the data logger to post data directly to a database the table(s) need to be defined in the database. The provided MySQL support will do this automatically when first used. Also, a utility is provided to generate a CREATE TABLE statement if you want to create the tables manually.

First step is to review and modify your filter so that it reflects the fields, their names and their order in the database. Then run the program to generate a CREATE TABLE statement and review that until you are satisfied.

1.  `startserver` **The server must be running to support the following programs**
2.  `buildfilter` to generate a default filter
3.  Edit the filter to define the fields you want and the order you want
4.  Setup all the mysql_xxx settings in conf/magnum-logger.conf and include, if a filter is wanted, logger_mysql_filtername=xxxx.
5.  Run `buildschema` to generate the CREATE TABLE code. Code to create an events table is also generated but is not needed unless your want to generate events.
6.  Review the generated CREATE TABLE code. You can now
    1.  Let the logger automatically create the tables needed or
    2.  Use the generate CREATE statements and your favourite MySQL utility to create the tables.

**NOTE:** It is important to get your filter set up as you wish as once you post data to a database it is not a trivial task to say "oops" and add columns.

## Database Logger Class Requirements

Database loggers are extended standard MagnumLoggers with extra methods:

It is defined as datalogger in conf file -  but must implement MagnumDBLog interface.

- init() no predefined action
- log() posts and/or logs inserts of data into database
- connect() called at beginning each transaction
- disconnect() called at end of each transaction
- postEvent(array $event) posts and/or logs event to database

Support classes are:

- the  global $server object of Magnum class for manipulating and filtering data
- The global $options array of configuration values
- The Schema class that contacts the server to get an array of data names and data types such as Byte, Short, Integer, Boolean, Float and String. can be used for schema generation and insert preparation.

How the class interfaces with the underlying database is irrelevant to the definition.

Review the MySQL implementation for an example.

#### MySQL Implementation

At release 2.0.3 the MySQL implementation was refactored to have only two program / class files.

- MySQL class (mysql.class.php). All this does is provide definitions of the table schemas based on the currently defined 'logger_mysql_filter' filter file. It is used by the buildschema tool as well as supporting LogDataSQL that automatically creates tables, if needed, during the connect process.
- LogDataSQL (logdatasql.class.php). This handles all MySQL support for both data logging and event logging.



# What the Programs Do

These are the scripts provide to start various logger side programs.
programs

| Program       | Description                                                  |
| ------------- | ------------------------------------------------------------ |
| buildschema   | Builds a MySQL schema definition based on your log record data. |
| buildfilter   | Generates a filter file with all data fields for your site   |
| startlogger   | Starts data logging as background task. This will restart a running logger. |
| stoplogger    | Stops background logger task                                 |
| showlogger    | Displays the logger status.                                  |
| runlogger     | Runs the datalogger in foreground. Useful for testing.       |
| enablelogger  | Enables the datalogger as a system service. (refer to RUNNING_AS_SERVICE document) |
| disablelogger | Disables the datalogger as a system service.                 |
| testserver    | Sends a single request to server to ensure its working       |