
/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 * support for filters was derived from this article
 * https://leonardom.wordpress.com/2009/08/06/getting-parameters-from-httpexchange/
 */
import java.net.InetSocketAddress;
import java.util.Properties;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import ca.godwin.magnum.MagnumHandler;
import ca.godwin.magnum.ParameterFilter;
import ca.godwin.magnum.ServerProperties;
import ca.godwin.magnum.TraceLogger;

public class MagnumServer {

	public static void main(String args[]) {
		Properties options = ServerProperties.getOptions();
		ServerProperties.addOptions(args);
		int port = 19450;
		boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
		try {
			port = Integer.parseInt(options.getProperty("dataserverport", "19450"));
		} catch (NumberFormatException e) {
		}
		if (trace) {
			TraceLogger.logit(String.format("Listening on port:%d", port));
		}
		try {
			HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
			ParameterFilter handler = new ParameterFilter();
			HttpContext context = server.createContext("/", new MagnumHandler());
			context.getFilters().add(handler);
			server.start();
		} catch (Throwable tr) {
			tr.printStackTrace();
		}
	}

}