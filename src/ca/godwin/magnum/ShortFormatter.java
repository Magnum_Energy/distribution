/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */package ca.godwin.magnum;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class ShortFormatter extends Formatter {
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss Z ");
	private final Properties options = ServerProperties.getOptions();
	private final boolean systemd = Boolean.parseBoolean(options.getProperty("systemd", "false"));

	public ShortFormatter() {
		super();
	}

	@Override
	public String format(LogRecord rec) {
		final StringBuffer buf = new StringBuffer(1000);
		if (!systemd) {
			buf.append(sdf.format(new Date()));
		}
		final Level level = rec.getLevel();
		if (level.intValue() > Level.INFO.intValue()) {
			buf.append(level.toString());
			String result = rec.getSourceClassName();
			if (result != null) {
				buf.append(' ');
				buf.append(result);
				result = rec.getSourceMethodName();
				if (result != null) {
					buf.append('#');
					buf.append(result);
				}
			}
		}
		if (rec.getMessage() != null) {
			buf.append(formatMessage(rec));
		}
		buf.append('\n');
		final Throwable thrown = rec.getThrown();
		if (thrown != null) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw);
			thrown.printStackTrace(pw);
			pw.close();
			buf.append(sw.getBuffer());
			buf.append('\n');
		}
		return buf.toString();
	}
}