package ca.godwin.magnum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONReader implements DataReader {
	protected Properties options = ServerProperties.getOptions();
	protected String jsonFileName = options.getProperty("jsonreader_filename", "");
	protected Gson gson = new GsonBuilder().serializeNulls().create();
	protected File jsonFile = null;
	protected BufferedReader jsonReader = null;

	public JSONReader() throws Exception {
		super();
	}

	public void init() throws Exception {
		jsonFile = new File(jsonFileName);
		if (!jsonFile.exists()) {
			jsonFile = null;
		} else {
			jsonReader = new BufferedReader(new FileReader(jsonFile));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashMap<String, Object> getData() throws Exception {
		String jsonString = null;
		if (jsonFile != null) {
			while (jsonString == null) {
				jsonString = jsonReader.readLine();
				// Just keep looping through the file.
				if (jsonString == null) {
					jsonReader.close();
					jsonReader = new BufferedReader(new FileReader(jsonFile));
				}
			}
		}
		if (jsonString != null && !jsonString.isEmpty()) {
			LinkedHashMap<String, Object> map = gson.fromJson(jsonString, LinkedHashMap.class);
			for (Entry<String, Object> entry : map.entrySet()) {
				Object obj = entry.getValue();
				String field = entry.getKey();
				/*
				 * We can't guess on Integer as JSON makes all numbers Float
				 */
				if (obj != null) {
					String objstring = obj.toString();
					try {
						obj = Float.parseFloat(objstring);
						map.replace(field, obj);
					} catch (Exception e) {
						try {
							switch (objstring.toLowerCase()) {
							case "true":
							case "false":
								obj = Boolean.parseBoolean(objstring);
								map.replace(field, obj);
								break;
							}
						} catch (Exception e1) {
						}
					}
				}
			}
			return map;

		} else {
			return new LinkedHashMap<String, Object>();
		}
	}
}
