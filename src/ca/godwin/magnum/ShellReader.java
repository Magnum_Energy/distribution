package ca.godwin.magnum;

/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
  This file is part of ca.godwin.magnum.
  ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
/* 
* This program will iterate through a semi colon delimited list of shell commands
* It expects each command to emit JSON
* The JSON will be absorbed and passed back to the main reader / handler
* No value added will be done on the jsonb. The data is expected to be LinkedHashMap compatible JSON
 *  
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ShellReader implements DataReader {
	protected Gson gson = new GsonBuilder().serializeNulls().create();
	private final Properties options = ServerProperties.getOptions();
	boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	private String commandlist = options.getProperty("shellreader_commands", "");
	private String[] commands;

	public ShellReader() throws Exception {
		super();
		commands = commandlist.split(";");
		for (int ix = 0; ix < commands.length; ix++) {
			commands[ix] = commands[ix].trim();
		}
	}

	@Override
	public LinkedHashMap<String, Object> getData() throws Exception {
		LinkedHashMap<String, Object> fullmap = new LinkedHashMap<>();
		for (String command : commands) {
			try {
				// using the Runtime exec method:
				Process job = Runtime.getRuntime().exec(command);
				int exit = job.waitFor();
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(job.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(job.getErrorStream()));
				// read the output from the command
				String stdout = "";
				String line;
				while ((line = stdInput.readLine()) != null) {
					stdout += line;
				}
				stdout = stdout.trim();
				stdInput.close();
				// read any errors from the attempted command
				@SuppressWarnings("unused")
				String stderr = "";
				while ((line = stdError.readLine()) != null) {
					stderr += line;
				}
				stdError.close();
				if (stdout.length() == 0 || exit != 0)
					TraceLogger.logit("Error encountered:" + stdout);
				else {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, Object> data = gson.fromJson(stdout, LinkedHashMap.class);
					for (Entry<String, Object> entry : data.entrySet()) {
						Object obj = entry.getValue();
						String field = entry.getKey();
						/*
						 * We can't guess on Integer as JSON makes all numbers Float
						 */
						if (obj != null) {
							String objstring = obj.toString();
							try {
								obj = Float.parseFloat(objstring);
								data.replace(field, obj);
							} catch (Exception e) {
								switch (objstring.toLowerCase()) {
								case "true":
								case "false":
									obj = Boolean.parseBoolean(objstring);
									data.replace(field, obj);
									break;
								}
							}
						}
					}
					fullmap.putAll(data);
				}
			} catch (IOException e) {
				TraceLogger.logit("exception happened ", e);
			}
		}
		return fullmap;
	}

	@Override
	public void init() throws Exception {
	}

	public static void main(String[] args) {
		ServerProperties.addOptions(args);
		try {
			DataReader reader = new ShellReader();
			LinkedHashMap<String, Object> map = reader.getData();
			if (map.size() > 0) {
				Gson gson = new GsonBuilder().serializeNulls().create();
				String json = gson.toJson(map, LinkedHashMap.class);
				System.out.println(json);
			} else {
				System.out.println("No data");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
