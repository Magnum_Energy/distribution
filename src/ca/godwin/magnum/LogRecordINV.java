/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class LogRecordINV extends MagnumRecord {
	private short INV_mode;
	private String INV_mode_text;
	private short INV_fault;
	private String INV_fault_text;
	private float INV_vdc;
	private short INV_adc;
	private short INV_VACout;
	private short INV_VACin;
	private byte INV_invled;
	private String INV_invled_text;
	private byte INV_chgled;
	private String INV_chgled_text;
	private String INV_revision;
	private short INV_bat;
	private short INV_tfmr;
	private short INV_fet;
	private short INV_model;
	private String INV_model_text;
	private short INV_stackmode;
	private String INV_stackmode_text;
	private short INV_AACin;
	private short INV_AACout;
	private float INV_Hz;

	public LogRecordINV() {
		super();
	}

	@Override
	public void processPacket(MagnumPacket packet) {
		if (packet.getPacketType() == MagnumMessage.INVERTER) {
			final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
			INV_mode = (short) Byte.toUnsignedInt(buf.get());
			INV_fault = (short) Byte.toUnsignedInt(buf.get());
			INV_vdc = (float) buf.getShort() / 10;
			INV_adc = buf.getShort();
			INV_VACout = (short) Byte.toUnsignedInt(buf.get());
			INV_VACin = (short) Byte.toUnsignedInt(buf.get());
			INV_invled = buf.get();
			INV_chgled = buf.get();
			MagnumRecord.inverter_revision = Byte.toUnsignedInt(buf.get());
			INV_revision = Float.toString((float)MagnumRecord.inverter_revision / 10);
			INV_bat = (short) Byte.toUnsignedInt(buf.get());
			INV_tfmr = (short) Byte.toUnsignedInt(buf.get());
			INV_fet = (short) Byte.toUnsignedInt(buf.get());
			INV_model = (short) Byte.toUnsignedInt(buf.get()); // ignore for now s/b 6B
			INV_stackmode = (short) Byte.toUnsignedInt(buf.get()); // ignore stack mode
			INV_AACin = (short) Byte.toUnsignedInt(buf.get());
			INV_AACout = (short) Byte.toUnsignedInt(buf.get());
			INV_Hz = (float) buf.getShort() / 10;
			// The last byte is ignorwd and sometimes is not present
			/*
			 * (Model <= 50) means 12V inverter
			   (Model <= 107) means 24V inverter
			   (Model < 150) means 48V inverter
			 */
			if (INV_model <= 50) {
				MagnumRecord.setVoltage(12);
			} else if (INV_model <= 107) {
				MagnumRecord.setVoltage(24);
			} else if (INV_model <= 150) {
				MagnumRecord.setVoltage(48);
			}
			MagnumRecord.inverter_model = INV_model;
			setINV_fault_text();
			setINV_chgled_text();
			setINV_invled_text();
			setINV_mode_text();
			setINV_model_text();
			setINV_stackmode_text();
		}
	}

	private void setINV_model_text() {
		switch (INV_model) {
		case 6:
			INV_model_text = "MM612";
			break;
		case 7:
			INV_model_text = "MM612-AE";
			break;
		case 8:
			INV_model_text = "MM1212";
			break;
		case 9:
			INV_model_text = "MMS1012";
			break;
		case 10:
			INV_model_text = "MM1012E";
			break;
		case 11:
			INV_model_text = "MM1512";
			break;
		case 12:
			INV_model_text = "MMS912E";
			break;
		case 15:
			INV_model_text = "ME1512";
			break;
		case 20:
			INV_model_text = "ME2012";
			break;
		case 21:
			INV_model_text = "RD2212";
			break;
		case 25:
			INV_model_text = "ME2512";
			break;
		case 30:
			INV_model_text = "ME3112";
			break;
		case 35:
			INV_model_text = "MS2012";
			break;
		case 36:
			INV_model_text = "MS1512E";
			break;
		case 40:
			INV_model_text = "MS2012E";
			break;
		case 44:
			INV_model_text = "MSH3012M";
			break;
		case 45:
			INV_model_text = "MS2812";
			break;
		case 47:
			INV_model_text = "MS2712E";
			break;
		case 53:
			INV_model_text = "MM1324E";
			break;
		case 54:
			INV_model_text = "MM1524";
			break;
		case 55:
			INV_model_text = "RD1824";
			break;
		case 59:
			INV_model_text = "RD2624E";
			break;
		case 63:
			INV_model_text = "RD2824";
			break;
		case 69:
			INV_model_text = "RD4024E";
			break;
		case 74:
			INV_model_text = "RD3924";
			break;
		case 90:
			INV_model_text = "MS4124E";
			break;
		case 91:
			INV_model_text = "MS2024";
			break;
		case 103:
			INV_model_text = "MSH4024M";
			break;
		case 104:
			INV_model_text = "MSH4024RE";
			break;
		case 105:
			INV_model_text = "MS4024";
			break;
		case 106:
			INV_model_text = "MS4024AE";
			break;
		case 107:
			INV_model_text = "MS4024PAE";
			break;
		case 111:
			INV_model_text = "MS4448AE";
			break;
		case 112:
			INV_model_text = "MS3748AEJ";
			break;
		case 114:
			INV_model_text = "MS4048";
			break;
		case 115:
			INV_model_text = "MS4448PAE";
			break;
		case 116:
			INV_model_text = "MS3748PAEJ";
			break;
		case 117:
			INV_model_text = "MS4348PE";
			break;
		default:
			INV_model_text = "Unknown";
			break;
		}
	}

	private void setINV_stackmode_text() {
		switch (INV_stackmode) {
		case 0x00:
			INV_stackmode_text = "Stand Alone";
			break;
		case 0x01:
			INV_stackmode_text = "Parallel stack - master";
			break;
		case 0x02:
			INV_stackmode_text = "Parallel stack - slave";
			break;
		case 0x04:
			INV_stackmode_text = "Series stack - master";
			break;
		case 0x08:
			INV_stackmode_text = "Series stack - slave";
			break;
		default:
			INV_stackmode_text = "Unknown";
			break;
		}

	}

	protected void setINV_invled_text() {
		INV_invled_text = INV_invled == 0 ? "Off" : "On";
	}

	protected void setINV_chgled_text() {
		INV_chgled_text = INV_chgled == 0 ? "Off" : "On";
	}

	protected void setINV_mode_text() {
		switch (INV_mode) {
		case 0x00:
			INV_mode_text = "Standby";
			break;
		case 0x01:
			INV_mode_text = "EQ";
			break;
		case 0x02:
			INV_mode_text = "FLOAT";
			break;
		case 0x04:
			INV_mode_text = "ABSORB";
			break;
		case 0x08:
			INV_mode_text = "BULK";
			break;
		case 0x09:
			INV_mode_text = "BATSAVER";
			break;
		case 0x10:
			INV_mode_text = "CHARGE";
			break;
		case 0x20:
			INV_mode_text = "Off";
			break;
		case 0x40:
			INV_mode_text = "INVERT";
			break;
		case 0x50:
			INV_mode_text = "Inverter_Standby";
			break;
		case (byte) 0x80:
			INV_mode_text = "SEARCH";
			break;
		default:
			INV_mode_text = "??";
		}
	}

	protected void setINV_fault_text() {
		INV_fault_text = "";
		switch (INV_fault) {
		case 0x00:
			INV_fault_text = "None";
			break;
		case 0x01:
			INV_fault_text = "STUCK RELAY";
			break;
		case 0x02:
			INV_fault_text = "DC OVERLOAD";
			break;
		case 0x03:
			INV_fault_text = "AC OVERLOAD";
			break;
		case 0x04:
			INV_fault_text = "DEAD BAT";
			break;
		case 0x05:
			INV_fault_text = "BACKFEED";
			break;
		case 0x08:
			INV_fault_text = "LOW BAT";
			break;
		case 0x09:
			INV_fault_text = "HIGH BAT";
			break;
		case 0x0A:
			INV_fault_text = "HIGH AC VOLTS";
			break;
		case 0x10:
			INV_fault_text = "BAD_BRIDGE";
			break;
		case 0x12:
			INV_fault_text = "NTC_FAULT";
			break;
		case 0x13:
			INV_fault_text = "FET_OVERLOAD";
			break;
		case 0x14:
			INV_fault_text = "INTERNAL_FAULT4";
			break;
		case 0x16:
			INV_fault_text = "STACKER MODE FAULT";
			break;
		case 0x18:
			INV_fault_text = "STACKER CLK PH FAULT";
			break;
		case 0x17:
			INV_fault_text = "STACKER NO CLK FAULT";
			break;
		case 0x19:
			INV_fault_text = "STACKER PH LOSS FAULT";
			break;
		case 0x20:
			INV_fault_text = "OVER TEMP";
			break;
		case 0x21:
			INV_fault_text = "RELAY FAULT";
			break;
		case 0x80:
			INV_fault_text = "CHARGER_FAULT";
			break;
		case 0x81:
			INV_fault_text = "High Battery Temp";
			break;
		case 0x90:
			INV_fault_text = "OPEN SELCO TCO";
			break;
		case 0x91:
			INV_fault_text = "CB3 OPEN FAULT";
			break;
		}
	}

	@Override
	public String getDevice() {
		return "INV";
	}

	@Override
	public String getVersion() {
		return INV_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("INV_revision", INV_revision);
		map.put("INV_mode", INV_mode);
		map.put("INV_mode_text", INV_mode_text);
		map.put("INV_fault", INV_fault);
		map.put("INV_fault_text", INV_fault_text);
		map.put("INV_vdc", INV_vdc);
		map.put("INV_adc", INV_adc);
		map.put("INV_VACout", INV_VACout);
		map.put("INV_VACin", INV_VACin);
		map.put("INV_invled", INV_invled);
		map.put("INV_invled_text", INV_invled_text);
		map.put("INV_chgled", INV_chgled);
		map.put("INV_chgled_text", INV_chgled_text);
		map.put("INV_bat", INV_bat);
		map.put("INV_tfmr", INV_tfmr);
		map.put("INV_fet", INV_fet);
		map.put("INV_model", INV_model);
		map.put("INV_model_text", INV_model_text);
		map.put("INV_stackmode", INV_stackmode);
		map.put("INV_stackmode_text", INV_stackmode_text);
		map.put("INV_AACin", INV_AACin);
		map.put("INV_AACout", INV_AACout);
		map.put("INV_Hz", INV_Hz);
		return map;
	}

}