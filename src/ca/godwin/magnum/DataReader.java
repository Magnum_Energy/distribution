/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */

/*
 * This is the interface to use for plugins. How you genratye the MagnumRecord(s) is
 * independent of the exisitng system.
 */
package ca.godwin.magnum;

import java.util.LinkedHashMap;

public interface DataReader {
	
	void init() throws Exception;

	/*
	 * Return an ordered array of name / value pairs
	 */
	
	LinkedHashMap<String, Object> getData() throws Exception;

}