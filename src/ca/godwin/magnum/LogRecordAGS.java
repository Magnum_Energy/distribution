/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class LogRecordAGS extends MagnumRecord {
	private byte AGS_status;
	private String AGS_status_text = "unknown";
	private String AGS_revision = "0.0";
	private short AGS_temp;
	private float AGS_runtime;
	private float AGS_vdc;
	private boolean AGS_running;

	private int AGS_gen_last_run = -1;
	private int AGS_last_full_soc = -1; // not available in some devices
	private int AGS_gen_total_run = -1; // not available in some devices

	public LogRecordAGS() {
		super();
	}

	/* (non-Javadoc)
	 * @see ca.godwin.magnum.DeviceInterface#extractValues(ca.godwin.magnum.MagnumPacket)
	 */
	@Override
	public void processPacket(MagnumPacket packet) {
		final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
		switch (packet.getPacketType()) {
		case AGS_A1:
			buf.get(); // skip lead byte
			AGS_status = buf.get();
			AGS_revision = String.valueOf((float) Byte.toUnsignedInt(buf.get()) / 10);
			AGS_temp = buf.get();
			/**
			 * AGS_temp is unsigned in ARC and not valid below 33 F and 104 F I recommend
			 * using a separate sensor for full range of temperatures that are below 0 celcius
			 */
			if (AGS_temp < 105) {
				AGS_temp = (short) (((float) AGS_temp - 32) * 5 / 9);
			}
			AGS_runtime = (float)  Byte.toUnsignedInt(buf.get()) / 10;
			setStatusText();
			if (buf.hasRemaining()) {
				int value_vdc = Byte.toUnsignedInt(buf.get()) * voltage_multiplier;
				AGS_vdc = (float) value_vdc / 10;
			}
			setRunning();
			break;
		case AGS_A2:
			buf.get(); // skip first
			AGS_gen_last_run = Byte.toUnsignedInt(buf.get());
			AGS_last_full_soc =  Byte.toUnsignedInt(buf.get());
			AGS_gen_total_run = Short.toUnsignedInt(buf.getShort());
			break;
		default:
			break;
		}
	}

	private void setRunning() {

		switch (AGS_status) {
		case 3:
		case 6:
		case 7:
		case 8:
		case 12:
		case 13:
		case 14:
		case 18:
		case 19:
		case 26:
		case 27:
			AGS_running = true;
			break;
		default:
			AGS_running = false;
		}
	}

	private void setStatusText() {
		switch (AGS_status) {
		case 0:
			AGS_status_text = "Not Connected";
			break;
		case 1:
			AGS_status_text = "Off";
			break;
		case 2:
			AGS_status_text = "Ready";
			break;
		case 3:
			AGS_status_text = "Manual Run";
			break;
		case 4:
			AGS_status_text = "AC In";
			break;
		case 5:
			AGS_status_text = "In quiet time";
			break;
		case 6:
			AGS_status_text = "Start in test mode";
			break;
		case 7:
			AGS_status_text = "Start on temperature";
			break;
		case 8:
			AGS_status_text = "Start on voltage";
			break;
		case 9:
			AGS_status_text = "Fault start on test";
			break;
		case 10:
			AGS_status_text = "Fault start on temp";
			break;
		case 11:
			AGS_status_text = "Fault start on voltage";
			break;
		case 12:
			AGS_status_text = "Start TOD";
			break;
		case 13:
			AGS_status_text = "Start SOC";
			break;
		case 14:
			AGS_status_text = "Start Exercise";
			break;
		case 15:
			AGS_status_text = "Fault start TOD";
			break;
		case 16:
			AGS_status_text = "Fault start SOC";
			break;
		case 17:
			AGS_status_text = "Fault start Exercise";
			break;
		case 18:
			AGS_status_text = "Start on Amp";
			break;
		case 19:
			AGS_status_text = "Start on Topoff";
			break;
		case 20:
			AGS_status_text = "Not used";
			break;
		case 21:
			AGS_status_text = "Fault start on Amp";
			break;
		case 22:
			AGS_status_text = "Fault on Topoff";
			break;
		case 23:
			AGS_status_text = "Not used";
			break;
		case 24:
			AGS_status_text = "Fault max run";
			break;
		case 25:
			AGS_status_text = "Gen Run Fault";
			break;
		case 26:
			AGS_status_text = "Gen in Warm up";
			break;
		case 27:
			AGS_status_text = "Gen in Cool down";
			break;
		default:
			AGS_status_text = "Unknown";
		}
	}

	public String getStatusText() {
		return AGS_status_text;
	}

	@Override
	public String getDevice() {
		return "AGS";
	}

	@Override
	public String getVersion() {
		return AGS_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("AGS_revision", AGS_revision);
		map.put("AGS_status", AGS_status);
		map.put("AGS_status_text", AGS_status_text);
		map.put("AGS_running", AGS_running);
		map.put("AGS_temp", AGS_temp);
		map.put("AGS_runtime", AGS_runtime);
		map.put("AGS_gen_last_run", AGS_gen_last_run);
		map.put("AGS_last_full_soc", AGS_last_full_soc);
		map.put("AGS_gen_total_run", AGS_gen_total_run);
		map.put("AGS_vdc", AGS_vdc);
		return map;
	}

}