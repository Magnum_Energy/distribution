/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class LogRecordPT100 extends MagnumRecord {
	byte PT_address;
	boolean PT_on_off;
	byte PT_mode;
	String PT_mode_text;
	String PT_mode_hex;
	byte PT_regulation;
	String PT_regulation_text;
	String PT_status_byte;
	byte PT_fault;
	String PT_fault_text;
	float PT_battery;
	float PT_battery_amps;
	float PT_pv_voltage;
	float PT_charge_time;
	float PT_target_battery_voltage;
	boolean PT_relay_state;
	boolean PT_alarm_state;
	float PT_battery_temperature;
	float PT_inductor_temperature;
	float PT_fet_temperature;

	int PT_lifetime_kwhrs;
	float PT_resettable_kwhrs;
	short PT_ground_fault_current;
	short PT_nominal_battery_voltage;
	byte PT_stacker_info;
	String PT_revision;
	short PT_model;
	short PT_output_current_rating;
	short PT_input_voltage_rating;

	short PT_record;
	float PT_daily_kwh;
	short PT_max_daily_pv_volts;
	float PT_max_daily_pv_volts_time;
	short PT_max_daily_battery_volts;
	float PT_max_daily_battery_volts_time;
	short PT_minimum_daily_battery_volts;
	float PT_minimum_daily_battery_volts_time;
	float PT_daily_time_operational;
	short PT_daily_amp_hours;
	short PT_peak_daily_power;
	float PI_peak_daily_power_time;

	short PT_fault_number;
	short PT_max_battery_volts;
	short PT_max_pv_to_battery_vdc;
	short PT_max_battery_temperature;
	short PT_max_fet_temperature;
	short PT_max_inductor_temperature;

	public LogRecordPT100() {
		super();
	}

	@Override
	public void processPacket(MagnumPacket packet) {
		final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
		int byte_value;
		int short_value;
		switch(packet.getPacketType()) {
		case PT_C1:
			buf.get(); // skip header
			PT_address = (byte)(buf.get() >> 5);
			byte_value = Byte.toUnsignedInt(buf.get());
			// byte 2
			PT_on_off = (byte_value & 0x01) != 0?true:false;
			PT_mode =(byte)((byte_value >> 1) & 0x07);
			PT_regulation =(byte)((byte_value >>4) & 0x0F);
			/*
			self.data['on_off'] = True if (byte_value & 0x01) != 0 else False
            self.data['mode'] = byte_value >> 1 & 0x07
            self.data['mode_hex'] = hex(unpacked[2]).upper()
            self.data['regulation'] = byte_value >> 4 & 0x0F
			 */
			PT_mode_hex = String.format("0X%02X", byte_value);
			// byte 3
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_fault = (byte) (byte_value >> 3);
			byte[] status_byte = {(byte) byte_value};
			PT_status_byte = MagnumPacket.bytesToHex(status_byte);
			PT_battery =(float) Short.toUnsignedInt(buf.getShort()) / 10;
			PT_battery_amps =(float) buf.getShort() / 10;
			PT_pv_voltage =(float) buf.getShort() / 10;
			PT_charge_time =(float) buf.get() / 10;
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_target_battery_voltage =(float)(( byte_value / 10) * voltage_multiplier);
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_relay_state =((byte_value &(byte) 0x80) >> 7) != 0?true:false;
			PT_alarm_state = ((byte_value &(byte) 0x70) >> 6) != 0?true:false;
			// changed to match python
			byte_value = buf.get();
			PT_battery_temperature = (float) (((float) byte_value / 10) + 25.0); // based on 25 C base
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_inductor_temperature =  (float) byte_value ;
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_fet_temperature =  (float)( byte_value );
			switch (PT_mode) {
			case 2:
				PT_mode_text = "Sleep";
				break;
			case 3:
				PT_mode_text = "Float";
				break;
			case 4:
				PT_mode_text = "Bulk";
				break;
			case 5:
				PT_mode_text = "Absorb";
				break;
			case 6:
				PT_mode_text = "EQ";
				break;
			case 0:
			default:
				PT_mode_text = "Unknown";
				break;
			}

			switch (PT_regulation) {
			case 0:
				PT_regulation_text = "Off";
				break;
			case 1:
				PT_regulation_text = "Voltage";
				break;
			case 2:
				PT_regulation_text = "Current";
				break;
			case 3:
				PT_regulation_text = "Temperature";
				break;
			case 4:
				PT_regulation_text = "Hardware";
				break;
			case 5:
				PT_regulation_text = "Voltage Off Limit";
				break;
			case 6:
				PT_regulation_text = "PPT Limit";
				break;
			case 7:
				PT_regulation_text = "Fault Limit";
				break;
			}
			switch (PT_fault) {
			case 0 : PT_fault_text="No Fault"; break;
			case 1 : PT_fault_text="Input Breaker Fault"; break;
			case 2 : PT_fault_text="Output Breaker Fault"; break;
			case 3 : PT_fault_text="PV High Fault"; break;
			case 4 : PT_fault_text="Battery High Fault"; break;
			case 5 : PT_fault_text="BTS Shorted Fault"; break;
			case 6 : PT_fault_text="FET Overtemp Fault"; break;
			case 7 : PT_fault_text="Inductor Overtemp Fault"; break;
			case 8 : PT_fault_text="Over Current Fault"; break;
			case 9 : PT_fault_text="Internal Phase Fault"; break;
			case 10: PT_fault_text="Repeated Internal Phase Fault"; break;
			case 11: PT_fault_text="Internal Fault 1"; break;
			case 12: PT_fault_text="GFP Fault"; break;
			case 13: PT_fault_text="ARC Fault"; break;
			case 14: PT_fault_text="NTC Fault"; break;
			case 15: PT_fault_text="FET Overload Fault"; break;
			case 16: PT_fault_text="Stack Fault 1"; break;
			case 17: PT_fault_text="Stack Fault 2"; break;
			case 18: PT_fault_text="Stack Fault 3"; break;
			case 19: PT_fault_text="High Battery Temp Fault"; break;
			default: PT_fault_text = "unknown"; break;
			}

			break;
		case PT_C2:
			buf.get(); // skip header
			PT_address =(byte)(buf.get() >> 5);
			PT_lifetime_kwhrs = Short.toUnsignedInt(buf.getShort()) * 10;
			PT_resettable_kwhrs =(float) Short.toUnsignedInt(buf.getShort()) / 10;
			PT_ground_fault_current = (short) Byte.toUnsignedInt(buf.get());
			byte_value = buf.get();
			PT_nominal_battery_voltage =(byte)(byte_value >> 2);
			PT_stacker_info =(byte)(byte_value & 0x03);
			PT_revision = Float.toString((float) Byte.toUnsignedInt(buf.get()) / 10);
			PT_model = (short) Byte.toUnsignedInt(buf.get());
			PT_output_current_rating = (short) Byte.toUnsignedInt(buf.get());
			PT_input_voltage_rating = (short) (Byte.toUnsignedInt(buf.get()) * 10);
			break;
		case PT_C3:
			buf.get(); // skip header
			short_value = Short.toUnsignedInt(buf.getShort());
			PT_address =(byte)((short_value & 0xE000) >> 13);
			PT_record =(short)(short_value & 0x1ffffff);
			PT_daily_kwh =(float) Byte.toUnsignedInt(buf.get()) / 10;
			PT_max_daily_pv_volts = (short) Byte.toUnsignedInt(buf.get());
			PT_max_daily_pv_volts_time =(float)  Byte.toUnsignedInt(buf.get()) / 10;
			PT_max_daily_battery_volts = (short) Byte.toUnsignedInt(buf.get());
			PT_max_daily_battery_volts_time =(float)  Byte.toUnsignedInt(buf.get()) / 10;
			PT_minimum_daily_battery_volts = (short) Byte.toUnsignedInt(buf.get());
			PT_minimum_daily_battery_volts_time =(float) (short) Byte.toUnsignedInt(buf.get()) / 10;
			PT_daily_time_operational =(float) Byte.toUnsignedInt(buf.get()) / 10;
			PT_daily_amp_hours = (short) Byte.toUnsignedInt(buf.get());
			PT_peak_daily_power = (short) Byte.toUnsignedInt(buf.get());
			PI_peak_daily_power_time =(float) (short) Byte.toUnsignedInt(buf.get()) / 10;
			break;
		case PT_C4:
			buf.get(); // skip header
			byte_value = Byte.toUnsignedInt(buf.get());
			PT_address = (byte) ((byte_value & 0xE0) >> 5);
			PT_fault_number = (short) (byte_value & 0x1f);
			PT_max_battery_volts = (short) Byte.toUnsignedInt(buf.get());
			PT_max_pv_to_battery_vdc =  (short) Byte.toUnsignedInt(buf.get());
			PT_max_battery_temperature = (short) Byte.toUnsignedInt(buf.get());
			PT_max_fet_temperature = (short) Byte.toUnsignedInt(buf.get());
			PT_max_inductor_temperature = (short) Byte.toUnsignedInt(buf.get());
			break;
		default:
			break;
		}
	}

	@Override
	public String getDevice() {
		return "PT100";
	}

	@Override
	public String getVersion() {
		return PT_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("PT_revision", PT_revision);
		map.put("PT_address", PT_address);
		map.put("PT_on_off", PT_on_off);
		map.put("PT_mode", PT_mode);
		map.put("PT_mode_text", PT_mode_text);
		map.put("PT_mode_hex", PT_mode_hex);
		map.put("PT_regulation", PT_regulation);
		map.put("PT_status_byte", PT_status_byte);
		map.put("PT_regulation_text", PT_regulation_text);
		map.put("PT_fault", PT_fault);
		map.put("PT_fault_text", PT_fault_text);
		map.put("PT_battery", PT_battery);
		map.put("PT_battery_amps", PT_battery_amps);
		map.put("PT_pv_voltage", PT_pv_voltage);
		map.put("PT_charge_time", PT_charge_time);
		map.put("PT_target_battery_voltage", PT_target_battery_voltage);
		map.put("PT_relay_state", PT_relay_state);
		map.put("PT_alarm_state", PT_alarm_state);
		map.put("PT_battery_temperature", PT_battery_temperature);
		map.put("PT_inductor_temperature", PT_inductor_temperature);
		map.put("PT_fet_temperature", PT_fet_temperature);
		map.put("PT_lifetime_kwhrs", PT_lifetime_kwhrs);
		map.put("PT_resettable_kwhrs", PT_resettable_kwhrs);
		map.put("PT_ground_fault_current", PT_ground_fault_current);
		map.put("PT_nominal_battery_voltage", PT_nominal_battery_voltage);
		map.put("PT_stacker_info", PT_stacker_info);
		map.put("PT_model", PT_model);
		map.put("PT_output_current_rating", PT_output_current_rating);
		map.put("PT_input_voltage_rating", PT_input_voltage_rating);
		map.put("PT_record", PT_record);
		map.put("PT_daily_kwh", PT_daily_kwh);
		map.put("PT_max_daily_pv_volts", PT_max_daily_pv_volts);
		map.put("PT_max_daily_pv_volts_time", PT_max_daily_pv_volts_time);
		map.put("PT_max_daily_battery_volts", PT_max_daily_battery_volts);
		map.put("PT_max_daily_battery_volts_time", PT_max_daily_battery_volts_time);
		map.put("PT_minimum_daily_battery_volts", PT_minimum_daily_battery_volts);
		map.put("PT_minimum_daily_battery_volts_time", PT_minimum_daily_battery_volts_time);
		map.put("PT_daily_time_operational", PT_daily_time_operational);
		map.put("PT_daily_amp_hours", PT_daily_amp_hours);
		map.put("PT_peak_daily_power", PT_peak_daily_power);
		map.put("PI_peak_daily_power_time", PI_peak_daily_power_time);
		map.put("PT_fault_number", PT_fault_number);
		map.put("PT_max_battery_volts", PT_max_battery_volts);
		map.put("PT_max_pv_to_battery_vdc", PT_max_pv_to_battery_vdc);
		map.put("PT_max_battery_temperature", PT_max_battery_temperature);
		map.put("PT_max_fet_temperature", PT_max_fet_temperature);
		map.put("PT_max_inductor_temperature", PT_max_inductor_temperature);
		return map;

	}

}