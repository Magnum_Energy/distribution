/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class MagnumHandler implements HttpHandler {

	private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
	protected Properties options = ServerProperties.getOptions();
	protected boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	protected ArrayList<DataReader> readers = new ArrayList<>();
	private DateTimeFormatter formatter;
	private ZoneId zone;
	private final DecimalFormat rounded_float = new DecimalFormat("######.##");
	private final Gson gson = new GsonBuilder().serializeNulls().create();
	private LinkedHashMap<String, Object> schemamap = null;

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		if (trace)
			TraceLogger.logit("Entering " + this.getClass().getSimpleName());
		@SuppressWarnings("unchecked")
		Map<String, Object> params = (Map<String, Object>) exchange.getAttribute("parameters");
		String path = exchange.getRequestURI().getPath();
		int offset = path.lastIndexOf('/');
		String showpath = path.substring(offset);
		byte response[] = new byte[0];
		boolean schema = false;
		if (showpath.equals("/schema") && params.size() == 0) {
			schema = true;
		} else if (showpath.length() > 1 || params.size() > 0) {
			/*
			 * Only allow known endpoints and no query string
			 */
			response = "Service not available".getBytes("UTF-8");
			exchange.getResponseHeaders().add("Content-Type", "text/plain; charset=UTF-8");
			exchange.sendResponseHeaders(503, response.length);
			if (trace)
				TraceLogger.logit("Returning:" + new String(response));
			OutputStream out = exchange.getResponseBody();
			out.write(response);
			out.close();
			return;
		} 
		LinkedHashMap<String, Object> map = new LinkedHashMap<>(200, (float) 1.0);
		ZonedDateTime rightnow = ZonedDateTime.now(zone);
		map.put("Date", rightnow.format(formatter));
		String timezone = rightnow.getOffset().getId();
		if (timezone.equalsIgnoreCase("Z"))
			timezone = "+00:00";
		map.put("timezone", timezone);
		if ((!schema) || schemamap == null) {
			for (final DataReader reader : readers) {
				try {
					if (trace)
						TraceLogger.logit("Calling Reader:" + reader.getClass().getSimpleName());
					map.putAll(reader.getData());
					if (trace)
						TraceLogger.logit("Back from Reader:" + reader.getClass().getSimpleName());
				} catch (final Exception e) {
					if (trace)
						TraceLogger.logit("Reader exception", e);
					response = e.getMessage().getBytes("UTF-8");
					exchange.getResponseHeaders().add("Content-Type", "text/plain; charset=UTF-8");
					exchange.sendResponseHeaders(500, response.length);
					break;
				}
			}
		}
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		if (response.length == 0) {
			// update date fields in case modified by readers
			map.replace("Date", rightnow.format(formatter));
			map.replace("timezone", timezone);
			if (schemamap == null) {
				if (map.size() > 2) {
					schemamap = new LinkedHashMap<>(map.size());
					for (Entry<String, Object> entry : map.entrySet()) {
						Object obj = entry.getValue();
						String field = entry.getKey();
						schemamap.put(field,
								obj == null ? String.class.getSimpleName() : obj.getClass().getSimpleName());
					}
				} else {
					schema = false; // noop the schema call
				}
			}
			if (map.size() > 2 || schema) {
				String json;
				if (schema) {
					json = gson.toJson(schemamap, LinkedHashMap.class);
				} else {
					for (Entry<String, Object> entry : map.entrySet()) {
						// clean up float precision
						Object obj = entry.getValue();
						String field = entry.getKey();
						if (obj instanceof Float) {
							if (((Float) obj).isNaN()) {
								map.replace(field, null);
							} else {
								map.replace(field, new Float(rounded_float.format(obj)));
							}
						}
					}
					json = gson.toJson(map, LinkedHashMap.class);
				}
				response = json.getBytes("UTF-8");
				exchange.getResponseHeaders().add("Content-Type", "application/json; charset=UTF-8");
				exchange.sendResponseHeaders(200, response.length);
			} else {
				schemamap = null; // resets schema on break
				response = "No data generated".getBytes("UTF-8");
				exchange.getResponseHeaders().add("Content-Type", "text/plain; charset=UTF-8");
				exchange.sendResponseHeaders(503, response.length);
			}
		}

		if (trace)
			TraceLogger.logit("Returning:" + new String(response));
		OutputStream out = exchange.getResponseBody();
		out.write(response);
		out.close();
	}

	public MagnumHandler() throws Exception {
		super();
		/*
		 * Check validity of date and time zone
		 */
		this.zone = ZoneId.systemDefault();
		String value = options.getProperty("timezone", "default");
		if (value.length() > 0 && !value.equals("default")) {
			try {
				this.zone = ZoneId.of(value);
			} catch (Exception e) {
				TraceLogger.logit("Invalid Timezone (using default):" + value + " " + e.getMessage());
			}
		}
		String dateformat = options.getProperty("dateformat", DEFAULT_FORMAT);
		this.formatter = DateTimeFormatter.ofPattern(dateformat);
		ZonedDateTime rightnow = ZonedDateTime.now(zone);
		try {
			rightnow.format(formatter);
		} catch (Exception e) {
			TraceLogger.logit(dateformat + " is not valid", e);
			if (DEFAULT_FORMAT.equals(dateformat)) {
				throw (e);
			}
			this.formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
			TraceLogger.logit("Using default:" + DEFAULT_FORMAT);
		}

		final String plugins = options.getProperty("server_readers", "");
		ArrayList<String> pluginlist = new ArrayList<>();
		if (!plugins.isEmpty()) {
			final String[] classes = plugins.split(",");
			for (String classname : classes) {
				if (!pluginlist.contains(classname)) {
					pluginlist.add(classname.trim());
				}
			}
		}
		if (pluginlist.size() == 0) {
			pluginlist.add(MagnumReader.class.getName());
		}
		String default_package = this.getClass().getName();
		int ix = default_package.lastIndexOf(".");
		if (ix > 0) {
			default_package = default_package.substring(0, ix + 1);
		} else {
			default_package = "";
		}
		for (String classname : pluginlist) {
			Class<?> item = null;
			try {
				item = this.getClass().getClassLoader().loadClass(classname);
			} catch (final Exception e) {
				if (classname.contains(".") || default_package.length() == 0) {
					TraceLogger.logit("Error detected:" + classname, e);
					continue;
				} else {
					try {
						classname = default_package + classname;
						item = this.getClass().getClassLoader().loadClass(classname);
					} catch (final Exception e1) {
						TraceLogger.logit("Error detected:" + classname, e1);
						continue;
					}
				}
			}
			final Object object = item.newInstance();
			if (object instanceof DataReader) {
				((DataReader) object).init();
				readers.add((DataReader) object);
				if (trace) {
					TraceLogger.logit("Plugin loaded:" + classname);
				}
			} else {
				TraceLogger.logit(classname + " is not a valid plugin");
			}
		}

	}
}
