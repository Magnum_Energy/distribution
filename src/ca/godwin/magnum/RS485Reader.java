/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
/*
 * https://fazecast.github.io/jSerialComm/ for details
 */
package ca.godwin.magnum;

import java.util.Arrays;
import java.util.Properties;

import com.fazecast.jSerialComm.SerialPort;

public class RS485Reader implements PacketReader {
	final Properties options = ServerProperties.getOptions();
	boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	SerialPort reader = null;
	String device;
	private long bytestoread; // bufferRead size
	private int deviceix;

	public RS485Reader() {
		super();
	}

	public void initReader() {
		try {
			bytestoread = Long.parseLong(options.getProperty("bytestoread", "21"));
		} catch (final NumberFormatException e) {
			bytestoread = 21;
		}
		device = options.getProperty("datareaderdevice", "/dev/ttyUSB0");
		if (trace)
			TraceLogger.logit("Device=" + device);
		try {
			deviceix = Integer.parseInt(device);
		} catch (final NumberFormatException e) {
			deviceix = -1;
			SerialPort[] ports = SerialPort.getCommPorts();
			for (int ix = 0; ix < ports.length; ix++) {
				if (device.equals(ports[ix].getPortDescription()) || device.equals(ports[ix].getSystemPortName())) {
					deviceix = ix;
					break;
				}
			}
		}
		if (device.isEmpty()) {
			deviceix = 0;
		}
		if (deviceix >= 0) {
			device = "/dev/" + SerialPort.getCommPorts()[deviceix].getSystemPortName();
			options.setProperty("datareaderdevice", device);
		}
	}

	/*
	 * This method has been written for speed not elegance in order to get most good
	 * packets quickly
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see ca.godwin.magnum.PacketReader#getRawPackets(int)
	 */
	@Override
	public byte[][] getRawPackets(int packetcount) throws Exception {
		if (reader == null) {
			try {
				SerialPort[] ports = SerialPort.getCommPorts();
				if (ports.length == 0 || deviceix >= ports.length) {
					TraceLogger.logit("There are no available serial ports");
					reader = null;
					return new byte[0][];
				}
				if (deviceix >= 0) {
					reader = SerialPort.getCommPorts()[deviceix];
				} else {
					reader = SerialPort.getCommPort(device);
				}
				reader.setComPortParameters(19200, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
				reader.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
			} catch (Exception e) {
				TraceLogger.logit("Failure to define reader", e);
				reader = null;
				return new byte[0][];
			}
		}

		int resultix = 0;
		final byte[][] packets = new byte[packetcount][];
		int bytes = 0;
		final byte[] readBuffer = new byte[8192];
		int count;
		if (!reader.openPort(500)) {
			TraceLogger.logit("Failure to open USB device at " + device);
			reader = null;
			return new byte[0][]; // staying alive
		}
		Thread.sleep(500);
		if (reader.bytesAvailable() == 0) {
			TraceLogger.logit("there doesn't seem to be a network");
			reader = null;
			return new byte[0][];
		}
		while (packetcount > 0) {
			count = reader.readBytes(readBuffer, bytestoread, bytes);
			bytes += count;
			if (count == 0) {
				if (bytes > 0) {
					packets[resultix++] = Arrays.copyOf(readBuffer, bytes);
					bytes = 0;
					packetcount--;
				}
			} else if (count < 0) {
				reader.closePort();
				reader = null;
				throw new Exception("Read error occured after " + resultix + " packets");
			} else if (bytes + bytestoread > readBuffer.length)
				bytes -= bytestoread; // make just enough space
		}
		reader.closePort();
		return packets;
	}

	/**
	 * @return the device
	 */
	public String getDevice() {
		return device;
	}
}
