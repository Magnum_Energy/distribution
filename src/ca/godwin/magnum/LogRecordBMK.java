/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class LogRecordBMK extends MagnumRecord {
	private byte BMK_soc;
	private float BMK_vdc;
	private float BMK_adc;
	private float BMK_vmin;
	private float BMK_vmax;
	private int BMK_amph;
	private float BMK_amphtrip;
	private int BMK_amphout;
	private String BMK_revision;
	private short BMK_Fault;
	private String BMK_Fault_Text = "Undefined";

	public LogRecordBMK() {
		super();
	}

	@Override
	public void processPacket(MagnumPacket packet) {
		final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
		switch(packet.getPacketType()) {
		case BMK_81:
			buf.get();
			BMK_soc = buf.get();
			BMK_vdc = (float) Short.toUnsignedInt(buf.getShort()) / 100;
			BMK_adc = (float) buf.getShort() / 10;
			BMK_vmin = (float) Short.toUnsignedInt(buf.getShort()) / 100;
			BMK_vmax = (float) Short.toUnsignedInt(buf.getShort()) / 100;
			BMK_amph = buf.getShort();
			BMK_amphtrip = (float) Short.toUnsignedInt(buf.getShort()) / 10;
			BMK_amphout = Short.toUnsignedInt(buf.getShort()) * 100;
			BMK_revision = Float.toString((float) Byte.toUnsignedInt(buf.get())/ 10);
			// some devices only generate 17 byte packets instead of 18
			if (buf.capacity() > 17) {
				BMK_Fault = (short) Byte.toUnsignedInt(buf.get());
				switch (BMK_Fault) {
				case 0:
					BMK_Fault_Text = "Reserved";
					break;
				case 1:
					BMK_Fault_Text = "Normal";
					break;
				case 2:
					BMK_Fault_Text = "Fault Start";
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public String getDevice() {
		return "BMK";
	}

	@Override
	public String getVersion() {
		return BMK_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("BMK_revision", BMK_revision);
		map.put("BMK_soc", BMK_soc);
		map.put("BMK_vdc", BMK_vdc);
		map.put("BMK_adc", BMK_adc);
		map.put("BMK_vmin", BMK_vmin);
		map.put("BMK_vmax", BMK_vmax);
		map.put("BMK_amph", BMK_amph);
		map.put("BMK_amphtrip", BMK_amphtrip);
		map.put("BMK_amphout", BMK_amphout);
		map.put("BMK_Fault", BMK_Fault);
		map.put("BMK_Fault_Text", BMK_Fault_Text);
		return map;
	}
}