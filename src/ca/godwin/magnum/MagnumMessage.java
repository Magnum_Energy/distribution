/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

public enum MagnumMessage {
	UNKNOWN,
	DISCARDED,
	INVERTER,
	REMOTE_00,
	REMOTE_A0,
	REMOTE_A1,
	REMOTE_A2,
	REMOTE_A3,
	REMOTE_A4,
	REMOTE_80,
	REMOTE_11,
	REMOTE_C0,
	REMOTE_C1,
	REMOTE_C2,
	REMOTE_C3,
	REMOTE_D0, // unknown
	AGS_A1,
	AGS_A2,
	BMK_81,
	RTR_91,
	PT_C1,
	PT_C2,
	PT_C3,
	PT_C4,
	ACLD // not yet supported
}