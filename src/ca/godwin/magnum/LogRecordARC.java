/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.time.LocalTime;
import java.util.LinkedHashMap;

public class LogRecordARC extends MagnumRecord {
	/*
	 * action is a command byte and not useful for a log
	 */
	private byte ARC_action;
	private byte ARC_searchwatts;
	private short ARC_batterysize;
	// see extract method for a note
	private byte ARC_battype;
	private float ARC_absorb;
	private short ARC_chargeramps;
	private short ARC_ainput;
	private String ARC_revision;
	private short ARC_parallel;
	private short ARC_force_charge;
	private byte ARC_genstart;
	private float ARC_lbco;
	private short ARC_vaccutout;
	private float ARC_vsfloat;
	private float ARC_vEQ;
	private float ARC_absorbtime;
	// end of core info
	// A0
	private short ARC_remotetimehours;
	private short ARC_remotetimemins;
	private float ARC_runtime;
	private byte ARC_starttemp;
	private float ARC_startvdc;
	private short ARC_quiettime;
	// A1
	private short ARC_begintime;
	private short ARC_stoptime;
	private float ARC_vdcstop;
	private short ARC_voltstartdelay;
	private short ARC_voltstopdelay;
	private float ARC_maxrun;
	// A2
	private byte ARC_socstart;
	private byte ARC_socstop;
	private short ARC_ampstart;
	private short ARC_ampsstartdelay;
	private short ARC_ampstop;
	private short ARC_ampsstopdelay;
	// A3
	private short ARC_quietbegintime;
	private short ARC_quietendtime;
	private short ARC_exercisedays;
	private short ARC_exercisestart;
	private float ARC_exerciseruntime;
	private short ARC_topoff;
	// A4
	private short ARC_warmup;
	private short ARC_cool;
	// 80
	private byte ARC_batteryefficiency;
	private byte ARC_resetbmk;
	private short ARC_batsize;
	// 11 MSH - not supported
	private short ARC_mshinputamps;
	private short ARC_mshcutoutvoltage;
	// C0
	private byte ARC_forcechgode;
	private byte ARC_relayonoff;
	private byte ARC_buzzeronoff;
	private byte ARC_resetpt100;
	private byte ARC_address;
	private byte ARC_packet;
	private int ARC_lognumber;
	// C!
	private float ARC_relayonvdc;
	private float ARC_relayoffvdc;
	private short ARC_relayondelayseconds;
	private short ARC_relaydelayoffseconds;
	private short ARC_batterytempcomp;
	private short ARC_powersavetime;
	// C2
	private float ARC_alarmonvdc;
	private float ARC_alarmoffvdc;
	private short ARC_alarmdondelay;
	private short ARC_alarmoffdelay;
	private float ARC_eqdonetimer;
	private short ARC_chargerate;
	private byte ARC_rebulkonsunup;
	// C3
	private float ARC_AbsorbVoltage;
	private float ARC_FloatVoltage;
	private float ARC_EqualizeVoltage;
	private float ARC_RebulkVoltage;
	private short ARC_BatteryTemperatureCompensation;

	public LogRecordARC() {
		super();
	}

	@Override
	public void processPacket(MagnumPacket packet) {
		final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
		short value;
		byte byte_value;
		LocalTime time;
		switch (packet.getPacketType()) {
		case REMOTE_00:
			setBaseValues(buf);
			break;
		case REMOTE_A0:
			setBaseValues(buf);
			ARC_remotetimehours = (short) Byte.toUnsignedInt(buf.get());
			ARC_remotetimemins = (short) Byte.toUnsignedInt(buf.get());
			value = (short) Byte.toUnsignedInt(buf.get());
			ARC_runtime = (float) value / 10;
			ARC_starttemp = buf.get(); // this could have been a byte
			ARC_starttemp = (byte) (((float) ARC_starttemp - 32) * 5 / 9);
			value = (short) (Byte.toUnsignedInt(buf.get()) * MagnumRecord.voltage_multiplier);
			ARC_startvdc = (float) value / 10;
			ARC_quiettime = (short) Byte.toUnsignedInt(buf.get());
			break;
		case REMOTE_A1:
			setBaseValues(buf);
			ARC_begintime = (short) (Byte.toUnsignedInt(buf.get()) * 15);
			time = LocalTime.of(0, 0).plusMinutes(ARC_begintime);
			ARC_begintime = (short) (time.getHour() * 100 + time.getMinute());
			ARC_stoptime = (short) (Byte.toUnsignedInt(buf.get()) * 15);
			time = LocalTime.of(0, 0).plusMinutes(ARC_stoptime);
			ARC_stoptime = (short) (time.getHour() * 100 + time.getMinute());
			value = (short) Byte.toUnsignedInt(buf.get());
			ARC_vdcstop = value * MagnumRecord.voltage_multiplier / 10;
			ARC_voltstartdelay = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_voltstartdelay > 127) {
				ARC_voltstartdelay = (short) ((ARC_voltstartdelay & 0x0f) * 60);
			}
			ARC_voltstopdelay = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_voltstopdelay > 127) {
				ARC_voltstopdelay = (short) ((ARC_voltstopdelay & 0x0f) * 60);
			}
			ARC_maxrun = (float) Byte.toUnsignedInt(buf.get()) / 10;
			break;
		case REMOTE_A2:
			setBaseValues(buf);
			ARC_socstart = buf.get();
			ARC_socstop = buf.get();
			ARC_ampstart = (short) Byte.toUnsignedInt(buf.get());
			ARC_ampsstartdelay = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_ampsstartdelay > 127) {
				ARC_ampsstartdelay = (short) ((ARC_ampsstartdelay & 0x0f) * 60);
			}
			ARC_ampstop = (short) Byte.toUnsignedInt(buf.get());
			ARC_ampsstopdelay = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_ampsstopdelay > 127) {
				ARC_ampsstopdelay = (short) ((ARC_ampsstopdelay & 0x0f) * 60);
			}
			break;
		case REMOTE_A3:
			setBaseValues(buf);
			ARC_quietbegintime = (short) (Byte.toUnsignedInt(buf.get()) * 15);
			time = LocalTime.of(0, 0).plusMinutes(ARC_quietbegintime);
			ARC_quietbegintime = (short) (time.getHour() * 100 + time.getMinute());
			ARC_quietendtime = (short) (Byte.toUnsignedInt(buf.get()) * 15);
			time = LocalTime.of(0, 0).plusMinutes(ARC_quietendtime);
			ARC_quietendtime = (short) (time.getHour() * 100 + time.getMinute());
			ARC_exercisedays = (short) Byte.toUnsignedInt(buf.get());
			ARC_exercisestart = (short) (Byte.toUnsignedInt(buf.get()) * 15);
			time = LocalTime.of(0, 0).plusMinutes(ARC_exercisestart);
			ARC_exercisestart = (short) (time.getHour() * 100 + time.getMinute());
			ARC_runtime = (float) Byte.toUnsignedInt(buf.get()) / 10;
			ARC_topoff = (short) Byte.toUnsignedInt(buf.get());
			break;
		case REMOTE_A4:
			setBaseValues(buf);
			ARC_warmup = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_warmup > 127) {
				ARC_warmup = (short) ((ARC_warmup & 0x0f) * 60);
			}
			ARC_cool = (short) Byte.toUnsignedInt(buf.get());
			if (ARC_cool > 127) {
				ARC_cool = (short) ((ARC_cool & 0x0f) * 60);
			}
			break;
		case REMOTE_80:
			setBaseValues(buf);
			ARC_remotetimehours = buf.get();
			ARC_remotetimemins = buf.get();
			ARC_batteryefficiency = buf.get();
			ARC_resetbmk = buf.get();
			ARC_batsize = (short) (Byte.toUnsignedInt(buf.get()) * 10);
			break;
		case REMOTE_11:
			setBaseValues(buf);
			ARC_mshinputamps = (short) Byte.toUnsignedInt(buf.get());
			ARC_mshcutoutvoltage = (short) Byte.toUnsignedInt(buf.get());
			break;
		case REMOTE_C0:
			setBaseValues(buf);
			ARC_forcechgode = (byte) (buf.get() & 0x03);
			byte_value = buf.get();
			ARC_relayonoff = (byte) ((byte_value & 0x60) >> 6);
			ARC_buzzeronoff = (byte) ((byte_value & 0x30) >> 4);
			ARC_resetpt100 = buf.get();
			byte_value = buf.get();
			ARC_address = (byte) (byte_value >> 5);
			ARC_packet = (byte) (byte_value & 0x1f);
			ARC_lognumber = Short.toUnsignedInt(buf.getShort());
			break;
		case REMOTE_C1:
			setBaseValues(buf);
			// C!
			ARC_relayonvdc = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_relayoffvdc = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_relayondelayseconds = buf.get();
			if (ARC_relayondelayseconds < 0) {
				ARC_relayondelayseconds = (short) (60 * (0 - ARC_relayondelayseconds));
			}
			ARC_relaydelayoffseconds = buf.get();
			if (ARC_relaydelayoffseconds < 0) {
				ARC_relaydelayoffseconds = (short) (60 * (0 - ARC_relaydelayoffseconds));
			}
			ARC_batterytempcomp = buf.get();
			ARC_powersavetime = (short) (buf.get() >> 2);
			break;
		case REMOTE_C2:
			setBaseValues(buf);
			// C2
			ARC_alarmonvdc = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_alarmoffvdc = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_alarmdondelay = buf.get();
			if (ARC_alarmdondelay < 0) {
				ARC_alarmdondelay = (short) (60 * (0 - ARC_alarmdondelay));
			}
			ARC_alarmoffdelay = buf.get();
			if (ARC_alarmoffdelay < 0) {
				ARC_alarmoffdelay = (short) (60 * (0 - ARC_alarmoffdelay));
			}
			ARC_eqdonetimer = Byte.toUnsignedInt(buf.get()) / 10;
			byte_value = buf.get();
			ARC_chargerate = (short) ((byte_value & 0xFE) >> 1);
			ARC_rebulkonsunup = (byte) (byte_value & 0x01);
			break;
		case REMOTE_C3:
			setBaseValues(buf);
			// C3
			ARC_AbsorbVoltage = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_FloatVoltage = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_EqualizeVoltage = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			//ARC_AbsorbTime = Byte.toUnsignedInt(buf.get()) / 10;
			buf.get(); //skip
			ARC_RebulkVoltage = Byte.toUnsignedInt(buf.get()) / 10 * MagnumRecord.voltage_multiplier;
			ARC_BatteryTemperatureCompensation = buf.get();
			break;
		case REMOTE_D0: // undocumented - not suported
		case UNKNOWN:
		default:
			break;
		}
	}

	private void setBaseValues(ByteBuffer buf) {
		short value;
		ARC_action = buf.get();
		ARC_searchwatts = buf.get();
		ARC_batterysize = (short) Byte.toUnsignedInt(buf.get());
		/*
		 * This is an overloaded byte - see documentation if less than 100 it is battery
		 * type if over it is scaled to 0.1 and multiplied by multiples of 12
		 */
		value = (short) Byte.toUnsignedInt(buf.get());
		if (value > 100) {
			ARC_absorb = (float) (value * MagnumRecord.voltage_multiplier) / 10;
			ARC_battype = 0;
		} else {
			ARC_absorb = 0;
			ARC_battype = (byte) value;
		}
		ARC_chargeramps = (short) Byte.toUnsignedInt(buf.get());
		ARC_ainput = (short) Byte.toUnsignedInt(buf.get());
		ARC_revision = Float.toString((float) Byte.toUnsignedInt(buf.get()) / 10);
		value = (short) Byte.toUnsignedInt(buf.get());
		ARC_parallel = (short) (value & 0x0f);
		ARC_parallel = (short) (ARC_parallel * 10);
		ARC_force_charge = (short) (value & 0xf0);
		ARC_force_charge = (short) (ARC_force_charge >> 4);
		ARC_genstart = buf.get();
		ARC_lbco = (float) Byte.toUnsignedInt(buf.get()) / 10;
		ARC_vaccutout = (short) Byte.toUnsignedInt(buf.get());
		ARC_vsfloat = (float) Byte.toUnsignedInt(buf.get()) * MagnumRecord.voltage_multiplier / 10;
		ARC_vEQ = ARC_absorb + (float) Byte.toUnsignedInt(buf.get()) / 10;
		ARC_absorbtime = (float) Byte.toUnsignedInt(buf.get()) / 10;
	}

	@Override
	public String getDevice() {
		return "ARC";
	}

	@Override
	public String getVersion() {
		return ARC_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("ARC_revision", ARC_revision);
		map.put("ARC_action", ARC_action);
		map.put("ARC_searchwatts", ARC_searchwatts);
		map.put("ARC_batterysize", ARC_batterysize);
		// see extract method for a note // see extract method for a note
		map.put("ARC_battype", ARC_battype);
		map.put("ARC_absorb", ARC_absorb);
		map.put("ARC_chargeramps", ARC_chargeramps);
		map.put("ARC_ainput", ARC_ainput);
		map.put("ARC_parallel", ARC_parallel);
		map.put("ARC_force_charge", ARC_force_charge);
		map.put("ARC_genstart", ARC_genstart);
		map.put("ARC_lbco", ARC_lbco);
		map.put("ARC_vaccutout", ARC_vaccutout);
		map.put("ARC_vsfloat", ARC_vsfloat);
		map.put("ARC_vEQ", ARC_vEQ);
		map.put("ARC_absorbtime", ARC_absorbtime);
		// end of core info 
		// A0 
		map.put("ARC_remotetimehours", ARC_remotetimehours);
		map.put("ARC_remotetimemins", ARC_remotetimemins);
		map.put("ARC_runtime", ARC_runtime);
		map.put("ARC_starttemp", ARC_starttemp);
		map.put("ARC_startvdc", ARC_startvdc);
		map.put("ARC_quiettime", ARC_quiettime);
		// A1 
		map.put("ARC_begintime", ARC_begintime);
		map.put("ARC_stoptime", ARC_stoptime);
		map.put("ARC_vdcstop", ARC_vdcstop);
		map.put("ARC_voltstartdelay", ARC_voltstartdelay);
		map.put("ARC_voltstopdelay", ARC_voltstopdelay);
		map.put("ARC_maxrun", ARC_maxrun);
		// A2 
		map.put("ARC_socstart", ARC_socstart);
		map.put("ARC_socstop", ARC_socstop);
		map.put("ARC_ampstart", ARC_ampstart);
		map.put("ARC_ampsstartdelay", ARC_ampsstartdelay);
		map.put("ARC_ampstop", ARC_ampstop);
		map.put("ARC_ampsstopdelay", ARC_ampsstopdelay);
		// A3 
		map.put("ARC_quietbegintime", ARC_quietbegintime);
		map.put("ARC_quietendtime", ARC_quietendtime);
		map.put("ARC_exercisedays", ARC_exercisedays);
		map.put("ARC_exercisestart", ARC_exercisestart);
		map.put("ARC_exerciseruntime", ARC_exerciseruntime);
		map.put("ARC_topoff", ARC_topoff);
		// A4 
		map.put("ARC_warmup", ARC_warmup);
		map.put("ARC_cool", ARC_cool);
		// 80 
		map.put("ARC_batteryefficiency", ARC_batteryefficiency);
		map.put("ARC_resetbmk", ARC_resetbmk);
		map.put("ARC_batsize", ARC_batsize);
		// 11 MSH - not supported 
		map.put("ARC_mshinputamps", ARC_mshinputamps);
		map.put("ARC_mshcutoutvoltage", ARC_mshcutoutvoltage);
		// C0 
		map.put("ARC_forcechgode", ARC_forcechgode);
		map.put("ARC_relayonoff", ARC_relayonoff);
		map.put("ARC_buzzeronoff", ARC_buzzeronoff);
		map.put("ARC_resetpt100", ARC_resetpt100);
		map.put("ARC_address", ARC_address);
		map.put("ARC_packet", ARC_packet);
		map.put("ARC_lognumber", ARC_lognumber);
		// C1
		map.put("ARC_relayonvdc", ARC_relayonvdc);
		map.put("ARC_relayoffvdc", ARC_relayoffvdc);
		map.put("ARC_relayondelayseconds", ARC_relayondelayseconds);
		map.put("ARC_relaydelayoffseconds", ARC_relaydelayoffseconds);
		map.put("ARC_batterytempcomp", ARC_batterytempcomp);
		map.put("ARC_powersavetime", ARC_powersavetime);
		// C2 
		map.put("ARC_alarmonvdc", ARC_alarmonvdc);
		map.put("ARC_alarmoffvdc", ARC_alarmoffvdc);
		map.put("ARC_alarmdondelay", ARC_alarmdondelay);
		map.put("ARC_alarmoffdelay", ARC_alarmoffdelay);
		map.put("ARC_eqdonetimer", ARC_eqdonetimer);
		map.put("ARC_chargerate", ARC_chargerate);
		map.put("ARC_rebulkonsunup", ARC_rebulkonsunup);
		// C3
		map.put("ARC_AbsorbVoltage", ARC_AbsorbVoltage);
		map.put("ARC_FloatVoltage", ARC_FloatVoltage);
		map.put("ARC_EqualizeVoltage", ARC_EqualizeVoltage);
		map.put("ARC_RebulkVoltage", ARC_RebulkVoltage);
		map.put("ARC_BatteryTemperatureCompensation", ARC_BatteryTemperatureCompensation);
		return map;
	}

}