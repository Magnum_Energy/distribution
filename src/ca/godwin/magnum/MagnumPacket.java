/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.util.Arrays;
import java.util.Properties;

public class MagnumPacket {
	static final private byte[] sevenzeros = new byte[] { 0, 0, 0, 0, 0, 0, 0 };
	private static final char[] hexArray = "0123456789ABCDEF".toCharArray();
	Properties options = ServerProperties.getOptions();
	boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	private final byte[] savedbuffer;
	private MagnumMessage packetType;

	/*
	 * builds a hex string
	 */
	public static String bytesToHex(byte[] buffer) {
		final char[] hexChars = new char[buffer.length * 2];
		for (int j = 0; j < buffer.length; j++) {
			final int v = buffer[j] & 0xFF;
			hexChars[j * 2] = MagnumPacket.hexArray[v >>> 4];
			hexChars[j * 2 + 1] = MagnumPacket.hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String charToHex(char[] buffer) {
		final char[] hexChars = new char[buffer.length * 2];
		for (int j = 0; j < buffer.length; j++) {
			final int v = buffer[j] & 0xFF;
			hexChars[j * 2] = MagnumPacket.hexArray[v >>> 4];
			hexChars[j * 2 + 1] = MagnumPacket.hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static byte[] hexToBytes(String s) {
		final int len = s.length();
		final byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public MagnumPacket(byte[] buffer) throws Exception {
		if (buffer == null)
			throw new Exception("Byte array must not be null");
		int len = buffer.length;
		if (len == 22) { // there are issues with 22 byte packets
			len--;
			savedbuffer = Arrays.copyOf(buffer, len);
		} else {
			savedbuffer = buffer;
		}
		packetType = MagnumMessage.UNKNOWN;
		final int lastbyte = Byte.toUnsignedInt(buffer[len - 1]);
		final int firstbyte = Byte.toUnsignedInt(buffer[0]);
		switch (len) {
		case 2:
			if (firstbyte == 0x91) {
				packetType = MagnumMessage.RTR_91;
			}
			break;
		case 5: // real message length
		case 6: // documented message length
			switch (firstbyte) {
			case 0xa1:
				packetType = MagnumMessage.AGS_A1;
				break;
			case 0xa2:
				packetType = MagnumMessage.AGS_A2;
				break;
			}
			break;
		case 8: // documented message length
			if (firstbyte == 0xC4) {
				packetType = MagnumMessage.PT_C4;
			}
			break;
		case 12: // documented message length
		case 13: // sometimes shows up
			if (firstbyte == 0xC2) {
				packetType = MagnumMessage.PT_C2;
			}
			break;
		case 14: // documented message length
			if (firstbyte == 0xC3) {
				packetType = MagnumMessage.PT_C3;
			}
			break;
		case 16: // documented message length
			if (firstbyte == 0xC1) {
				packetType = MagnumMessage.PT_C1;
			}
			break;
		case 17: // real message length
		case 18: // documented message length
			if (firstbyte == 0x81) {
				packetType = MagnumMessage.BMK_81;
			}
			break;
		case 20: // some inverter packets are 20 bytes long
			// master = Byte.toUnsignedInt(buffer[15]); // only accept master inverters
			// if (master == 0x00 || master == 0x0 || master == 0x04) {
			packetType = MagnumMessage.INVERTER;
			// } else {
			// packetType = MagnumMessage.DISCARDED;
			// }
			break;
		case 21:
			final int version = Byte.toUnsignedInt(buffer[10]); // inverter revision
			final int model = Byte.toUnsignedInt(buffer[14]); // inverter model

			if (lastbyte == 0 && firstbyte == 0) {
				/**
				 * there is an undocumented Remote message generated with seven 0x00 bytes at
				 * the end. This code distinguishes it from a Inverter record with status byte 0
				 * = 0x00
				 */
				/**
				 * Also a ME-RC sends a spurious record with a zero end byte
				 */
				final byte[] tail = Arrays.copyOfRange(savedbuffer, 14, len);
				if (Arrays.equals(tail, MagnumPacket.sevenzeros)) { // all zeros for last seven bytes
					packetType = MagnumMessage.REMOTE_00;
				} else {
					if (version == MagnumRecord.inverter_revision && model == MagnumRecord.inverter_model
							|| MagnumRecord.inverter_revision == -1) {
						packetType = MagnumMessage.INVERTER;
					} else {
						packetType = MagnumMessage.REMOTE_00;
					}
					// master = buffer[15]; // only accept master inverters
					// if (master == 0x00 || master == 0x0 || master == 0x04) {
					// packetType = MagnumMessage.INVERTER;
					// } else {
					// packetType = MagnumMessage.DISCARDED;
					// }
				}
			} else {
				switch (lastbyte) {
				case 0:
					// packetType = MagnumMessage.INVERTER;
					if (version == MagnumRecord.inverter_revision && model == MagnumRecord.inverter_model
							|| MagnumRecord.inverter_revision == -1) {
						packetType = MagnumMessage.INVERTER;
						if (MagnumRecord.inverter_revision == -1) {
							MagnumRecord.inverter_revision = version;
							MagnumRecord.inverter_model = model;
						}
					} else {
						packetType = MagnumMessage.REMOTE_00;
					}
					break;
				case 0xa0:
					packetType = MagnumMessage.REMOTE_A0;
					break;
				case 0xa1:
					packetType = MagnumMessage.REMOTE_A1;
					break;
				case 0xa2:
					packetType = MagnumMessage.REMOTE_A2;
					break;
				case 0xa3:
					packetType = MagnumMessage.REMOTE_A3;
					break;
				case 0xa4:
					packetType = MagnumMessage.REMOTE_A4;
					break;
				case 0x80:
					packetType = MagnumMessage.REMOTE_80;
					break;
				case 0xC0:
					packetType = MagnumMessage.REMOTE_C0;
					break;
				case 0xC1:
					packetType = MagnumMessage.REMOTE_C1;
					break;
				case 0xC2:
					packetType = MagnumMessage.REMOTE_C2;
					break;
				case 0xC3:
					packetType = MagnumMessage.REMOTE_C3;
					break;
				case 0x11:
					packetType = MagnumMessage.REMOTE_11;
					break;
				case 0xD0: // undocumented
					packetType = MagnumMessage.REMOTE_D0;
					break;
				default:
					break;
				}
			}
		}
	}

	public MagnumMessage getPacketType() {
		return packetType;
	}

	public byte[] getBuffer() {
		return savedbuffer;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(packetType);
		builder.append("=>");
		builder.append(MagnumPacket.bytesToHex(savedbuffer));
		return builder.toString();
	}

	/**
	 * @param packetType
	 *            the packetType to set
	 */
	public void setPacketType(MagnumMessage packetType) {
		this.packetType = packetType;
	}

}