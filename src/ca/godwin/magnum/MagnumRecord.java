/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.util.Properties;

abstract public class MagnumRecord implements DeviceRecord {
	protected final Properties options = ServerProperties.getOptions();
	/*
	 * These are set by the first INV packet
	 */
	protected static int inverter_revision = -1; // needed to identify future remote packets
	protected static int inverter_model = -1; // needed to identify future remote packets
	protected static int system_voltage;
	protected static int voltage_multiplier;

	public MagnumRecord() {
		super();
	}
	
	abstract public void processPacket(MagnumPacket packet) throws Exception;

	protected static void setVoltage(int voltage) {
		if (voltage != MagnumRecord.system_voltage) {
			MagnumRecord.system_voltage = voltage;
			MagnumRecord.voltage_multiplier = MagnumRecord.system_voltage / 12;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Device)=");
		builder.append(getDevice());
		builder.append(", Version=");
		builder.append(getVersion());
		return builder.toString();
	}

}