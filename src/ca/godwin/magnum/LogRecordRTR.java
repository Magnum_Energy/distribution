/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;

public class LogRecordRTR extends MagnumRecord {

	public String RTR_revision;

	public LogRecordRTR() {
		super();
	}

	@Override
	public void processPacket(MagnumPacket packet) {
		if (packet.getPacketType() == MagnumMessage.RTR_91) {
			final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
			buf.get();
			RTR_revision = Float.toString((float) Byte.toUnsignedInt(buf.get()) / 10);
		}
	}

	@Override
	public String getDevice() {
		return "RTR";
	}

	@Override
	public String getVersion() {
		return RTR_revision;
	}

	@Override
	public LinkedHashMap<String, Object> getMap() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("RTR_revision", RTR_revision);
		return map;
	}
}