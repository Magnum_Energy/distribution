/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MagnumReader implements DataReader {
	// fields to drop if no device
	static final String[] noAGS = { "ARC_genstart", "ARC_runtime", "ARC_starttemp", "ARC_startvdc", "ARC_quiettime",
			"ARC_begintime", "ARC_stoptime", "ARC_vdcstop", "ARC_voltstartdelay", "ARC_voltstopdelay", "ARC_maxrun",
			"ARC_socstart", "ARC_socstop", "ARC_ampstart", "ARC_ampsstartdelay", "ARC_ampstop", "ARC_ampsstopdelay",
			"ARC_quietbegintime", "ARC_quietendtime", "ARC_exercisedays", "ARC_exercisestart",
			"ARC_exerciseruntime", "ARC_topoff", "ARC_warmup", "ARC_cool" };
	static final String[] noBMK = { "ARC_batteryefficiency", "ARC_resetbmk", "ARC_batsize" };
	static final String[] noMSH = { "ARC_mshinputamps", "ARC_mshcutoutvoltage" };
	static final String[] noPT100 = { "ARC_forcechgode", "ARC_relayonoff", "ARC_buzzeronoff", "ARC_resetpt100", "ARC_address",
			"ARC_packet", "ARC_lognumber", "ARC_relayonvdc", "ARC_relayoffvdc", "ARC_relayondelayseconds",
			"ARC_relaydelayoffseconds", "ARC_batterytempcomp", "ARC_powersavetime", "ARC_alarmonvdc",
			"ARC_alarmoffvdc", "ARC_alarmdondelay", "ARC_alarmoffdelay", "ARC_eqdonetimer", "ARC_chargerate",
			"ARC_rebulkonsunup", "ARC_AbsorbVoltage", "ARC_FloatVoltage", "ARC_EqualizeVoltage", "ARC_AbsorbTime",
			"ARC_RebulkVoltage", "ARC_BatteryTemperatureCompensation" };
	protected Properties options = ServerProperties.getOptions();
	protected boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
	protected int packetstride = Integer.parseInt(System.getProperty("packetstride", "50"));
	final boolean oldserialreader = Boolean.parseBoolean(options.getProperty("oldserialreader", "false"));
	protected PacketReader reader;
	/*
	 * I have moved these to being persistent for duration
	 * This is necessary as some peripherals are not very talkative
	 * The AGS doesn't always send an A2 packet
	 * and the PT-100 doesn't say very much at all about C2, C3 and C4 packets
	 */
	protected LogRecordINV INV_data = null;
	protected LogRecordARC ARC_data = null;
	protected LogRecordAGS AGS_data = null;
	protected LogRecordBMK BMK_data = null;
	protected LogRecordRTR RTR_data = null;
	protected LogRecordPT100 PT100_data = null;

	public MagnumReader() throws Exception {
		super();
		reader = oldserialreader?new RS485Reader():new RS485Reader_2();
	}
	
	public void init() throws Exception {
		reader.initReader();
	}

	protected ArrayList<DeviceRecord> consolidate(ArrayList<MagnumPacket> records) {
		for (final MagnumPacket packet : records) {
			try {
				final MagnumMessage type = packet.getPacketType();
				final ByteBuffer buf = ByteBuffer.wrap(packet.getBuffer());
				buf.limit(buf.capacity());
				buf.position(0);
				switch (type) {
				case INVERTER:
					if (INV_data == null) {
						INV_data = new LogRecordINV();
					}
					INV_data.processPacket(packet);
					break;
				case REMOTE_00:
				case REMOTE_A0:
				case REMOTE_A1:
				case REMOTE_A2:
				case REMOTE_A3:
				case REMOTE_A4:
				case REMOTE_80:
					if (ARC_data == null) {
						ARC_data = new LogRecordARC();
					}
					ARC_data.processPacket(packet);
					break;
				case AGS_A1:
				case AGS_A2:
					if (AGS_data == null) {
						AGS_data = new LogRecordAGS();
					}
					AGS_data.processPacket(packet);
					break;
				case BMK_81:
					if (BMK_data == null) {
						BMK_data = new LogRecordBMK();
					}
					BMK_data.processPacket(packet);
					break;
				case RTR_91:
					if (RTR_data == null) {
						RTR_data = new LogRecordRTR();
					}
					RTR_data.processPacket(packet);
					break;
				case PT_C1:
				case PT_C2:
				case PT_C3:
				case PT_C4:
					if (PT100_data == null) {
						PT100_data = new LogRecordPT100();
					}
					PT100_data.processPacket(packet);
					break;
				default:
					break;
				}
			} catch (Exception e) {
				String msg = packet.toString();
				TraceLogger.logit("Error processing packet:" + msg, e);
			}
		}

		final ArrayList<DeviceRecord> result = new ArrayList<>(6);
		if (INV_data != null) {
			result.add(INV_data);
		}
		if (ARC_data != null) {
			result.add(ARC_data);
		}
		if (AGS_data != null) {
			result.add(AGS_data);
		}
		if (BMK_data != null) {
			result.add(BMK_data);
		}
		if (RTR_data != null) {
			result.add(RTR_data);
		}
		if (PT100_data != null) {
			result.add(PT100_data);
		}
		return result;
	}

	@Override
	public LinkedHashMap<String, Object> getData() throws Exception {
		final ArrayList<MagnumPacket> records = new ArrayList<>(packetstride);
		if (trace)
			TraceLogger.logit("calling:" + reader.getClass().getSimpleName());
		final byte[][] packets = reader.getRawPackets(packetstride + 1);
		if (trace)
			TraceLogger.logit("Back from " + reader.getClass().getSimpleName());
		for (final byte[] buffer : packets) {
			if (buffer != null && buffer.length > 0) {
				final MagnumPacket packet = new MagnumPacket(buffer);
				if (packet.getPacketType() != MagnumMessage.UNKNOWN
						&& packet.getPacketType() != MagnumMessage.DISCARDED) {
					records.add(packet);
				}
			}
		}
		ArrayList<DeviceRecord> devices =  consolidate(records);
		LinkedHashMap<String, Object> map = new LinkedHashMap<>(200, (float)1.0);
		for (final DeviceRecord device : devices) {
			map.putAll(device.getMap());
		}
		ArrayList<String> dropItems = new ArrayList<>();
		if (!map.containsKey("MSH_revision")) { // MSH not implemented
			dropItems.addAll(Arrays.asList(noMSH));
		}
		if (!map.containsKey("AGS_revision")) {
			dropItems.addAll(Arrays.asList(noAGS));
		}
		if (!map.containsKey("BMK_revision")) {
			dropItems.addAll(Arrays.asList(noBMK));
		}
		if (!map.containsKey("PT_revision")) {
			dropItems.addAll(Arrays.asList(noPT100));
		}
		for (String drop : dropItems) {
			map.remove(drop);
		}	
		return map;
	}
	
	public static void main(String[] args) throws Exception {
		ServerProperties.addOptions(args);
		Properties runoptions = ServerProperties.getOptions();
		LinkedHashMap<String, Object> map = new LinkedHashMap<>(200, (float)1.0);
		boolean fulllist = Boolean.parseBoolean(runoptions.getProperty("full-list", "false"));
		if(fulllist) {
			final ArrayList<DeviceRecord> list = new ArrayList<>(13);
			list.add(new LogRecordINV());
			list.add(new LogRecordARC());
			list.add(new LogRecordAGS());
			list.add(new LogRecordBMK());
			list.add(new LogRecordRTR());
			list.add(new LogRecordPT100());
			for (final DeviceRecord device : list) {
				System.out.println("# Start of " + device.getDevice());
				for (String field : device.getMap().keySet())
					System.out.println(field);
				map.putAll(device.getMap());
			}
		} else {
			MagnumReader magreader = new MagnumReader();
			magreader.init();
			map = magreader.getData();			
		}
		final Gson gson = new GsonBuilder().serializeNulls().create();
		String json = gson.toJson(map, LinkedHashMap.class);
		System.out.println(json);
	}
		

}
