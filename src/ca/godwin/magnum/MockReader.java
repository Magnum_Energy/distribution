/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Properties;

public class MockReader implements PacketReader {
	Properties options = ServerProperties.getOptions();
	String datafilename = options.getProperty("packetfilereader_filename", "mockdata.log");
	ArrayList<byte[]> mockpackets = new ArrayList<>(1000);
	private int packetix = 0;

	@Override
	public byte[][] getRawPackets(int packetcount) throws Exception {
		final byte[][] packets = new byte[packetcount][];
		int resultix = 0;
		while (packetcount-- > 0) {
			if (packetix >= mockpackets.size()) {
				packetix = 0;
			}
			packets[resultix++] = mockpackets.get(packetix++);
		}
		return packets;
	}

	public MockReader() throws Exception {

	}

	@Override
	public void initReader() throws Exception {
		/*
		 * build list
		 */
		final File logname = new File(datafilename);
		if (logname.exists()) {
			final BufferedReader reader = new BufferedReader(new FileReader(logname));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				final int pos = line.indexOf("=>");
				if (pos >= 0 && !line.startsWith("#")) {
					final String item = line.substring(pos + 2);
					final byte[] bytes = MagnumPacket.hexToBytes(item);
					mockpackets.add(bytes);
				}
			}
			reader.close();
		} else {
			throw new Exception("Missing mock data file:" + datafilename);
		}
		mockpackets.trimToSize();
		if (mockpackets.size() == 0) {
			throw new Exception("no mock mockpackets");
		}
		
	}

	@Override
	public String getDevice() {
		return "Mock data";
	}

}
