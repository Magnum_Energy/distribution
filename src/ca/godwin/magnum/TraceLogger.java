/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */package ca.godwin.magnum;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class TraceLogger {
	public static Logger tracelogger = Logger.getGlobal();
	static {
		TraceLogger.tracelogger.setUseParentHandlers(false);
		final ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new ShortFormatter());
		handler.setLevel(Level.ALL);
		TraceLogger.tracelogger.addHandler(handler);
	}
	public static void logit(String string) {
		TraceLogger.logit(string, null);
	}

	public static void logit(String string, Throwable event) {
		final StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		int ix = 2;
		if (stack[2].getMethodName().equals("log")) {
			ix = 3;
		}
		final LogRecord record = new LogRecord(Level.INFO, string);
		record.setSourceClassName(stack[ix].getClassName());
		record.setSourceMethodName(stack[ix].getMethodName());
		record.setThrown(event);
		TraceLogger.tracelogger.log(record);
	}

}
