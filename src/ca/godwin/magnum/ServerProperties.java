/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
package ca.godwin.magnum;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

public class ServerProperties {
	static protected Properties options = new Properties();

	static {
		String value = System.getenv("systemd");
		if (value == null)
			value = "false";
		options.put("systemd", value);
		ServerProperties.loadOptions("conf/magnum-server.conf");
	}

	static protected void loadOptions(final String name) {
		if (name != null && !name.isEmpty()) {
			final File optionsfile = new File(name);
			if (optionsfile.isFile()) {
				try {
					final FileInputStream fs = new FileInputStream(optionsfile);
					ServerProperties.options.load(fs);
					fs.close();
					for (Entry<Object, Object> entry : options.entrySet()) {
						options.replace(entry.getKey(), ((String) entry.getValue()).replaceAll("^\"|\"$", ""));
					}
					String more = ServerProperties.options.getProperty("config", "");
					ServerProperties.options.remove("config");
					if (more != null && !more.isEmpty()) {
						loadOptions(more);
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @return the options
	 */
	public static Properties getOptions() {
		return ServerProperties.options;
	}

	public static void addOptions(String[] args) {
		final HashMap<String, String> parms = new HashMap<>();
		for (final String parm : args) {
			final String[] pair = parm.split("=");
			pair[0] = pair[0].trim().toLowerCase();
			parms.put(pair[0], pair.length == 1 ? "" : pair[1]);
		}
		/*
		 * Process "config" commandline override
		 */
		final String newoptions = parms.get("config");
		parms.remove("config");
		if (newoptions != null && newoptions.length() > 0) {
			ServerProperties.loadOptions(newoptions);
		}
		for (final Entry<String, String> entry : parms.entrySet()) {
			ServerProperties.options.put(entry.getKey(), entry.getValue());
		}
		boolean trace = Boolean.parseBoolean(options.getProperty("trace", "false"));
		if (trace) {
			for (Entry<Object, Object> option : options.entrySet()) {
				TraceLogger.logit(option.toString());
			}
		}
	}
}
