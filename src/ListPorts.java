
/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */
import com.fazecast.jSerialComm.SerialPort;

public class ListPorts {

	public static void main(String[] args) {
		final SerialPort[] ports = SerialPort.getCommPorts();
		int ix = 0;
		for (final SerialPort port : ports) {
			System.out.println(String.format("Index:%d Device:/dev/%s Name:%s", ix++, port.getSystemPortName(),port.getPortDescription()));		
		}
	}

}
