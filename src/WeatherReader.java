/*
This software was developed by Charles Godwin magnum@godwin.ca

You are free to use and reuse. This is an example program only.

In order to use this plugin you must get your own free appid from https://openweathermap.org/api

Then modify conf/magnum_server.options with these lines
for server_readers ADD the reader to the list or the others will be dropped

server_readers=WeatherReader
weather_appid=<your appid>
weather_units=imperial|metric
weather_latitude=
weather_longitude=

If this was compiled as part of test of a build envionment (see PLUGIN_BUILD.md) then you can test it using
java -classpath build/plugin.jar:lib/* WeatherReader weather_appid="<your appid>" weather_latitude=<your locaton> weather_longitude=<your location>

 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import ca.godwin.magnum.DataReader;
import ca.godwin.magnum.ServerProperties;
import ca.godwin.magnum.TraceLogger;

public class WeatherReader implements DataReader {
	Properties options;
	URL url = null;
	LinkedHashMap<String, Object> map = new LinkedHashMap<>(5, (float) 1.0);
	long lastdate = 0;
	Gson gson = new GsonBuilder().serializeNulls().create();

	@Override
	public LinkedHashMap<String, Object> getData() throws Exception {
		long newdate = System.currentTimeMillis() / 1000;
		// only refresh values every 10 seconds
		if (newdate - lastdate >= 600 && url != null) {
			map.clear();
			BufferedReader in;
			try {
				in = new BufferedReader(new InputStreamReader(url.openStream()));
				StringBuilder string = new StringBuilder();
				String inputLine;
				while ((inputLine = in.readLine()) != null)
					string.append(inputLine);
				in.close();
				@SuppressWarnings("unchecked")
				LinkedHashMap<String, Object> json = gson.fromJson(string.toString(), LinkedHashMap.class);
				@SuppressWarnings("unchecked")
				LinkedTreeMap<String, Object> main = (LinkedTreeMap<String, Object>) json.get("main");
				for (Entry<String, Object> entry : main.entrySet()) {
					map.put("WTHR_" + entry.getKey().replaceAll("-", " "), entry.getValue());
				}
			} catch (Exception e) {
				throw e;
			}
			lastdate = newdate;
		}
		return map;
	}

	public static void main(String[] args) throws Exception {
		ServerProperties.addOptions(args);
		WeatherReader reader = new WeatherReader();
		LinkedHashMap<String, Object> maplist = reader.getData();
		System.out.println(maplist);
		return;
	}

	@Override
	public void init() throws Exception {
		options = ServerProperties.getOptions();
		String appid = options.getProperty("weather_appid", "");
		String units = options.getProperty("weather_units", "metric");
		String latitude = options.getProperty("weather_latitude");
		String longitude = options.getProperty("weather_longitude");
		if (appid == "") {
			TraceLogger.logit("Missing appid");
		} else {
			try {
				url = new URL(
						String.format("http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&units=%s&appid=%s",	latitude, longitude, units, appid));
			} catch (MalformedURLException e) {
				TraceLogger.logit("Could not establish weather URL", e);
			}
		}
	}

}
