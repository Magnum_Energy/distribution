
/*
This software was developed by Charles Godwin magnum@godwin.ca

Copyright (c) 2018
   This file is part of ca.godwin.magnum.
 ca.godwin.magnum is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  ca.godwin.magnum is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with ca.godwin.magnum.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.Properties;

import ca.godwin.magnum.MagnumPacket;
import ca.godwin.magnum.MockReader;
import ca.godwin.magnum.PacketReader;
import ca.godwin.magnum.RS485Reader;
import ca.godwin.magnum.RS485Reader_2;
import ca.godwin.magnum.ServerProperties;

public class Test_rs485 {

	public static void main(String args[]) throws Exception {
		ServerProperties.addOptions(args);
		final Properties options = ServerProperties.getOptions();
		final int packetlimit = Integer.parseInt(options.getProperty("packets", "100"));
		final float time = packetlimit * 46 / 1000;
		System.out.println("# ... There will be delay of about " + time + " seconds while " + packetlimit
				+ " packets are collected");
		// create an instance of the serial communications class
		final boolean mockdata = Boolean.parseBoolean(options.getProperty("mockdata", "false"));
		final boolean oldserialreader = Boolean.parseBoolean(options.getProperty("oldserialreader", "false"));
		PacketReader reader;
		if (mockdata) {
			reader = new MockReader();
		} else {
			reader = oldserialreader?new RS485Reader():new RS485Reader_2();
		}
		reader.initReader();
		final long start = System.currentTimeMillis();
		final byte[][] results = reader.getRawPackets(packetlimit);
		final long interval = System.currentTimeMillis() - start;
		long bytes = 0;
		System.out.println("using " + reader.getDevice() + (oldserialreader?" with OLD Serial reader":""));
		if (results.length == 0) {
			System.out.println("There was no data read");
		} else {
			for (final byte[] bytevalue : results) {
				if (bytevalue != null && bytevalue.length > 0) {
					final MagnumPacket packet = new MagnumPacket(bytevalue);
					final byte[] buffer = packet.getBuffer();
					bytes += buffer.length;
					System.out.println("Bytes read:" + buffer.length + ":" + packet.toString());
				} else {
					System.out.println("No data for this read - probably not connected to network");
				}
			}
			System.out.print("# " + (float) (interval / 1000) + " seconds, " + results.length + " packets, " + bytes + " bytes ");
		}

		System.exit(0);
	}
}
